package enhire.database.entities;

public enum EndavaFunction {
    HR, FRONTEND_DEV, BACKEND_DEV, TESTER, MANAGER;

    public static EndavaFunction fromString(final String value) {
        if (value == null) return null;
        switch (value.toUpperCase().trim()) {
            case "HR":
                return HR;
            case "FRONTEND_DEV":
                return FRONTEND_DEV;
            case "BACKEND_DEV":
                return BACKEND_DEV;
            case "TESTER":
                return TESTER;
            case "MANAGER":
                return MANAGER;
            default:
                return null;
        }
    }
}
