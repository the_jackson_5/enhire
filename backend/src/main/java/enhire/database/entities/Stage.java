package enhire.database.entities;

import org.joda.time.DateTime;

public class Stage {
    private StageEnum stage_code;
    private DateTime start_timestamp;
    private DateTime end_timestamp;

    private GradeEnum grade;
    private String employee_id;

    public StageEnum getStage_code() {
        return stage_code;
    }

    public void setStage_code(final StageEnum stage_code) {
        this.stage_code = stage_code;
    }

    public DateTime getStart_timestamp() {
        return start_timestamp;
    }

    public void setStart_timestamp(final DateTime start_timestamp) {
        this.start_timestamp = start_timestamp;
    }

    public DateTime getEnd_timestamp() {
        return end_timestamp;
    }

    public void setEnd_timestamp(final DateTime end_timestamp) {
        this.end_timestamp = end_timestamp;
    }

    public GradeEnum getGrade() {
        return grade;
    }

    public void setGrade(final GradeEnum grade) {
        this.grade = grade;
    }

    public String getEmployee_id() {
        return employee_id;
    }

    public void setEmployee_id(final String employee_id) {
        this.employee_id = employee_id;
    }
}
