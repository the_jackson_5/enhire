package enhire.database.entities;

/**
 * Created by slucutar on 4/16/2016.
 */
public enum GradeEnum {
    JUNIOR, SENIOR
}
