package enhire.database.entities;

import org.springframework.data.annotation.Id;

import javax.annotation.Generated;
import java.util.ArrayList;
import java.util.List;

public class Interview {
    @Id
    @Generated(value = "AUTO")
    private String _id;

    private List<Stage> stages;
    // TODO: add CV
    private EndavaFunction function;

    // When a candidate applies for a job, the interview_result has to be set to
    // pending
    private InterviewResult interview_result;

    public Interview() {
        stages = new ArrayList<>();
        interview_result = InterviewResult.PENDING;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(final String _id) {
        this._id = _id;
    }

    public List<Stage> getStages() {
        return stages;
    }

    public void setStages(final List<Stage> stages) {
        this.stages = stages;
    }

    public InterviewResult getInterview_result() {
        return interview_result;
    }

    public void setInterview_result(final InterviewResult interview_result) {
        this.interview_result = interview_result;
    }

    public EndavaFunction getFunction() {
        return function;
    }

    public void setFunction(final EndavaFunction function) {
        this.function = function;
    }
}
