package enhire.database.entities.collections;

import enhire.database.entities.Interview;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.annotation.Generated;
import java.util.ArrayList;
import java.util.List;

@Document(collection = "candidates")
public class Candidate {
    @Id
    @Generated(value = "AUTO")
    private String _id;

    private String first_name;
    private String last_name;
    private String email;
    private String phone;

    private final List<Interview> previous_applications;
    private Interview current_application;

    public Candidate() {
        this.previous_applications = new ArrayList<>();
    }

    public String get_id() {
        return _id;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(final String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(final String last_name) {
        this.last_name = last_name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(final String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(final String phone) {
        this.phone = phone;
    }

    public List<Interview> getPrevious_applications() {
        return previous_applications;
    }

    public Interview getCurrent_application() {
        return current_application;
    }

    public void setCurrent_application(final Interview current_application) {
        this.current_application = current_application;
    }
}
