package enhire.database.entities.collections;

import com.fasterxml.jackson.annotation.JsonIgnore;
import enhire.data_structures.Tuple;
import enhire.database.entities.EndavaFunction;
import enhire.database.entities.InterviewerFinishedInterview;
import org.joda.time.DateTime;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.annotation.Generated;
import java.util.ArrayList;
import java.util.List;

@Document(collection = "interviewers")
public class Interviewer {
    @Id
    @Generated(value = "AUTO")
    private String _id;

    private String first_name;
    private String last_name;
    private String email;
    private String phone;
    private String password;

    private EndavaFunction function;

    // first in tuple is a Candidate id, second is date and time for scheduled
    // interview
    private List<Tuple<String, DateTime>> pending_interviews;

    private List<InterviewerFinishedInterview> finished_interviews;

    public Interviewer() {
        pending_interviews = new ArrayList<>();
        finished_interviews = new ArrayList<>();
    }

    public String get_id() {
        return _id;
    }

    public void set_id(final String _id) {
        this._id = _id;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(final String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(final String last_name) {
        this.last_name = last_name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(final String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(final String phone) {
        this.phone = phone;
    }

    public EndavaFunction getFunction() {
        return function;
    }

    public void setFunction(final EndavaFunction function) {
        this.function = function;
    }

    public List<Tuple<String, DateTime>> getPending_interviews() {
        return pending_interviews;
    }

    public void setPending_interviews(final List<Tuple<String, DateTime>> pending_interviews) {
        this.pending_interviews = pending_interviews;
    }

    public List<InterviewerFinishedInterview> getFinished_interviews() {
        return finished_interviews;
    }

    public void setFinished_interviews(final List<InterviewerFinishedInterview> finished_interviews) {
        this.finished_interviews = finished_interviews;
    }

    @JsonIgnore
    public String getPassword() {
        return password;
    }

    public void setPassword(final String password) {
        this.password = password;
    }
}
