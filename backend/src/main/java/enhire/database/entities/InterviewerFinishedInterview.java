package enhire.database.entities;

import org.joda.time.DateTime;

public class InterviewerFinishedInterview {
    private String candidate_id;
    private DateTime timestamp;
    private String candidate_feedback;
    private Integer score;

    public String getCandidate_id() {
        return candidate_id;
    }

    public void setCandidate_id(final String candidate_id) {
        this.candidate_id = candidate_id;
    }

    public DateTime getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(final DateTime timestamp) {
        this.timestamp = timestamp;
    }

    public String getCandidate_feedback() {
        return candidate_feedback;
    }

    public void setCandidate_feedback(final String candidate_feedback) {
        this.candidate_feedback = candidate_feedback;
    }

    public Integer getScore() {
        return score;
    }

    public void setScore(final Integer score) {
        if (score < 0) this.score = 0;
        else if (score > 5) this.score = 5;
        else this.score = score;
    }

    @Override
    public String toString() {
        return "InterviewerFinishedInterview{" +
                "candidate_id='" + candidate_id + '\'' +
                ", timestamp=" + timestamp +
                ", candidate_feedback='" + candidate_feedback + '\'' +
                ", score=" + score +
                '}';
    }
}
