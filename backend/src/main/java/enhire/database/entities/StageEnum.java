package enhire.database.entities;

/**
 * Describes different phases of an interview.
 */
public enum StageEnum {
    PENDING, HR_DONE, TECHNICAL_DONE, FINAL_DONE
}
