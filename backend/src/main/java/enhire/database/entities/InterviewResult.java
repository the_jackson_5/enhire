package enhire.database.entities;

public enum InterviewResult {
    ACCEPTED, REJECTED, PENDING;

    public static InterviewResult fromString(final String value) {
        switch (value.toUpperCase().trim()) {
            case "ACCEPTED":
                return ACCEPTED;
            case "REJECTED":
                return REJECTED;
            case "PENDING":
                return PENDING;
            default:
                return null;
        }
    }
}
