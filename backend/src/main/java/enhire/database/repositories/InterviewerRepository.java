package enhire.database.repositories;

import enhire.database.entities.collections.Interviewer;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface InterviewerRepository extends MongoRepository<Interviewer, String> {
    @Query(value = "{\"email\": ?0}")
    List<Interviewer> findByEmail(final String email);
}
