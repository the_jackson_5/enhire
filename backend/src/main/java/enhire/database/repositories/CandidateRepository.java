package enhire.database.repositories;

import enhire.database.entities.collections.Candidate;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CandidateRepository extends MongoRepository<Candidate, String> {
    @Query(value = "{\"email\": ?0}")
    List<Candidate> findByEmail(final String email);
}
