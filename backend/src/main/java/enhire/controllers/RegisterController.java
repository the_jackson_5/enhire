package enhire.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import enhire.controllers.model.RESTResponseEnum;
import enhire.controllers.model.requests.RegisterCandidateRequest;
import enhire.database.entities.EndavaFunction;
import enhire.database.entities.Interview;
import enhire.database.entities.collections.Candidate;
import enhire.database.repositories.CandidateRepository;
import enhire.utils.RESTResponseUtils;
import org.apache.commons.validator.routines.EmailValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Created by slucutar on 4/16/2016.
 */
@RestController
public class RegisterController {

    @Autowired
    private CandidateRepository candidateRepository;

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public @ResponseBody String registerCandidate(@RequestBody final String body,
            final HttpServletResponse httpResponse) {
        final ObjectMapper om = new ObjectMapper();
        RegisterCandidateRequest request = null;
        try {
            request = om.readValue(body, RegisterCandidateRequest.class);
        } catch (final IOException e) {
            httpResponse.setStatus(RESTResponseEnum.INTERNAL_SERVER_ERROR.getStatus().value());
            return RESTResponseUtils.getRESTResponseAsString(RESTResponseEnum.INTERNAL_SERVER_ERROR);
        }

        if (request.hasAnyNullOrEmpty() || EndavaFunction.fromString(request.getPosition()) == null) {
            final RESTResponseEnum response = RESTResponseEnum.BAD_REQUEST;
            response.setMessage("Empty fields are not allowed.");
            httpResponse.setStatus(response.getStatus().value());
            return RESTResponseUtils.getRESTResponseAsString(response);
        }
        if (!EmailValidator.getInstance().isValid(request.getEmail())) {
            httpResponse.setStatus(RESTResponseEnum.INVALID_EMAIL.getStatus().value());
            return RESTResponseUtils.getRESTResponseAsString(RESTResponseEnum.INVALID_EMAIL);
        }

        final List<Candidate> candidates = candidateRepository.findByEmail(request.getEmail());
        if (candidates.size() != 0) {
            httpResponse.setStatus(RESTResponseEnum.CANDIDATE_ALREADY_EXISTS.getStatus().value());
            return RESTResponseUtils.getRESTResponseAsString(RESTResponseEnum.CANDIDATE_ALREADY_EXISTS);
        }

        final Candidate candidate = new Candidate();
        candidate.setEmail(request.getEmail());
        candidate.setFirst_name(request.getFirst_name());
        candidate.setLast_name(request.getLast_name());
        candidate.setPhone(request.getPhone());

        final Interview interview = new Interview();
        interview.setFunction(EndavaFunction.fromString(request.getPosition()));

        candidate.setCurrent_application(interview);

        candidateRepository.insert(candidate);
        httpResponse.setStatus(RESTResponseEnum.CANDIDATE_REGISTER_SUCCESS.getStatus().value());

        return RESTResponseUtils.getRESTResponseAsString(RESTResponseEnum.CANDIDATE_REGISTER_SUCCESS);
    }
}
