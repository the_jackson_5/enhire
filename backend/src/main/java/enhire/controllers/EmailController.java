package enhire.controllers;

import enhire.controllers.model.RESTResponseEnum;
import enhire.services.EmailSenderService;
import enhire.utils.RESTResponseUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.mail.MessagingException;
import javax.servlet.http.HttpServletResponse;

@RestController
public class EmailController {
    @Autowired
    EmailSenderService emailSenderService;

    @RequestMapping(value = "/send_email", method = RequestMethod.POST)
    public String sendEmail(final HttpServletResponse httpResponse) {
        try {
            emailSenderService.sendEmail("to@to.to", "from@from.from", "message", "title");
        } catch (final MessagingException e) {
            httpResponse.setStatus(RESTResponseEnum.EMAIL_ERROR.getStatus().value());
            return RESTResponseUtils.getRESTResponseAsString(RESTResponseEnum.EMAIL_ERROR);
        }
        return "";
    }
}
