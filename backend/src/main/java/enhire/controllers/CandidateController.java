package enhire.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import enhire.controllers.model.RESTResponseEnum;
import enhire.controllers.model.requests.CandidateFeedbackForInterviewerRequest;
import enhire.controllers.model.requests.FinalizeInterviewRequest;
import enhire.data_structures.Tuple;
import enhire.database.entities.*;
import enhire.database.entities.collections.Candidate;
import enhire.database.entities.collections.Interviewer;
import enhire.database.repositories.CandidateRepository;
import enhire.database.repositories.InterviewerRepository;
import enhire.utils.RESTResponseUtils;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by slucutar on 4/16/2016.
 */
@RestController
public class CandidateController {

    @Autowired
    private CandidateRepository candidateRepository;

    @Autowired
    private InterviewerRepository interviewerRepository;

    @RequestMapping(value = "/candidates/{id}", method = RequestMethod.GET)
    public @ResponseBody String getCandidateById(@PathVariable(value = "id") final String id,
            final HttpServletResponse httpResponse) {

        final Candidate candidate = candidateRepository.findOne(id);

        if (candidate != null) {
            httpResponse.setStatus(HttpStatus.OK.value());
            return RESTResponseUtils.getRESTResponseAsString(candidate);
        } else {
            httpResponse.setStatus(RESTResponseEnum.CANDIDATE_NOT_FOUND.getStatus().value());
            return RESTResponseUtils.getRESTResponseAsString(RESTResponseEnum.CANDIDATE_NOT_FOUND);
        }
    }

    @RequestMapping(value = "/candidates", method = RequestMethod.GET)
    public @ResponseBody String getCandidatesAndFilter(@RequestParam(value = "status") String status, @RequestParam(
            value = "interviewer_id", required = false) final String interviewerId,
            final HttpServletResponse httpResponse) {
        if (interviewerId == null || interviewerId.isEmpty()) {
            status = "pending";
        }

        final List<Candidate> result = new ArrayList<>();
        final List<Candidate> dbCandidates = candidateRepository.findAll();
        switch (status.toLowerCase().trim()) {
        case "pending":
            for (final Candidate candidate : dbCandidates) {
                if (isPending(candidate)) {
                    result.add(candidate);
                }
            }
            break;
        case "processed":
            for (final Candidate candidate : dbCandidates) {
                System.out.println(dbCandidates.size() + "  <-----");
                if (isCandidateProcessedBy(candidate, interviewerId)) {
                    result.add(candidate);
                }
            }
            break;
        case "finalized":
            for (final Candidate candidate : dbCandidates) {
                if (isFinalizedCandidate(candidate, interviewerId)) {
                    result.add(candidate);
                }
            }
            break;
        }
        httpResponse.setStatus(RESTResponseEnum.REQUEST_OK.getStatus().value());
        return RESTResponseUtils.getRESTResponseAsString(result);
    }

    @RequestMapping(value = "/candidates/{id}/feedback", method = RequestMethod.POST)
    public @ResponseBody String candidateFeedback(@RequestBody final String body, @PathVariable(
            value = "id") final String id, final HttpServletResponse httpResponse) {
        final ObjectMapper om = new ObjectMapper();
        CandidateFeedbackForInterviewerRequest request = null;
        try {
            request = om.readValue(body, CandidateFeedbackForInterviewerRequest.class);
        } catch (final IOException e) {
            httpResponse.setStatus(RESTResponseEnum.INTERNAL_SERVER_ERROR.getStatus().value());
            return RESTResponseUtils.getRESTResponseAsString(RESTResponseEnum.INTERNAL_SERVER_ERROR);
        }

        if (request.hasNullOrEmpty()) {
            httpResponse.setStatus(RESTResponseEnum.BAD_REQUEST.getStatus().value());
            return RESTResponseUtils.getRESTResponseAsString(RESTResponseEnum.BAD_REQUEST);
        }

        final Candidate candidate = candidateRepository.findOne(id);
        if (candidate == null) {
            httpResponse.setStatus(RESTResponseEnum.CANDIDATE_NOT_FOUND.getStatus().value());
            return RESTResponseUtils.getRESTResponseAsString(RESTResponseEnum.CANDIDATE_NOT_FOUND);
        }

        boolean foundInterview = false;
        for (final Stage stage : candidate.getCurrent_application().getStages()) {
            if (stage.getEmployee_id().matches(request.getInterviewer_id())) {
                stage.setGrade(GradeEnum.valueOf(request.getGrade().toUpperCase()));
                foundInterview = true;
                break;
            }
        }

        if (foundInterview) {
            httpResponse.setStatus(RESTResponseEnum.REQUEST_OK.getStatus().value());
            return RESTResponseUtils.getRESTResponseAsString(RESTResponseEnum.REQUEST_OK);
        } else {
            final RESTResponseEnum response = RESTResponseEnum.CANDIDATE_DID_NOT_PARTICIPATE;
            response.setMessage(response.getMessage().getMessage());
            httpResponse.setStatus(response.getStatus().value());
            return RESTResponseUtils.getRESTResponseAsString(response);
        }
    }

    @RequestMapping(value = "/candidates/{id}/interview", method = RequestMethod.POST)
    public String finalizeInterview(@RequestBody final String body, @PathVariable(
            value = "id") final String candidate_id, final HttpServletResponse httpResponse) {
        if (candidate_id == null || candidate_id.isEmpty()) {
            httpResponse.setStatus(RESTResponseEnum.BAD_REQUEST.getStatus().value());
            return RESTResponseUtils.getRESTResponseAsString(RESTResponseEnum.BAD_REQUEST);
        }

        final ObjectMapper om = new ObjectMapper();
        FinalizeInterviewRequest request = null;
        try {
            request = om.readValue(body, FinalizeInterviewRequest.class);
        } catch (final IOException e) {
            httpResponse.setStatus(RESTResponseEnum.INTERNAL_SERVER_ERROR.getStatus().value());
            return RESTResponseUtils.getRESTResponseAsString(RESTResponseEnum.INTERNAL_SERVER_ERROR);
        }

        final InterviewResult interviewResult = InterviewResult.fromString(request.getFinal_answer());
        if (interviewResult == null) {
            System.out.println(request.getFinal_answer());
            httpResponse.setStatus(RESTResponseEnum.BAD_REQUEST.getStatus().value());
            return RESTResponseUtils.getRESTResponseAsString(RESTResponseEnum.BAD_REQUEST);
        }

        final Candidate candidate = candidateRepository.findOne(candidate_id);
        if (candidate == null) {
            httpResponse.setStatus(RESTResponseEnum.CANDIDATE_NOT_FOUND.getStatus().value());
            return RESTResponseUtils.getRESTResponseAsString(RESTResponseEnum.CANDIDATE_NOT_FOUND);
        }

        if (candidate.getCurrent_application() == null) {
            httpResponse.setStatus(RESTResponseEnum.CANDIDATE_NOT_ENGAGED_CURRENTLY_IN_INTERVIEW.getStatus().value());
            return RESTResponseUtils.getRESTResponseAsString(
                    RESTResponseEnum.CANDIDATE_NOT_ENGAGED_CURRENTLY_IN_INTERVIEW);
        }

        final Interviewer interviewer = interviewerRepository.findOne(request.getInterviewer_id());
        if (interviewer == null || interviewer.getFunction() != EndavaFunction.HR) {
            httpResponse.setStatus(RESTResponseEnum.USER_UNAUTHORIZED.getStatus().value());
            return RESTResponseUtils.getRESTResponseAsString(RESTResponseEnum.USER_UNAUTHORIZED);
        }

        final Interview currentInterview = candidate.getCurrent_application();
        candidate.setCurrent_application(null);
        currentInterview.setInterview_result(interviewResult);
        candidate.getPrevious_applications().add(currentInterview);
        candidateRepository.save(candidate);

        int indx = -1;
        for (int i = 0; i < interviewer.getPending_interviews().size(); i++) {
            if (interviewer.getPending_interviews().get(i).getFirst().equals(candidate_id)) {
                indx = i;
                break;
            }
        }
        if (indx != -1) {
            final Tuple<String, DateTime> intrv = interviewer.getPending_interviews().get(indx);
            interviewer.getPending_interviews().remove(indx);
            final InterviewerFinishedInterview finished = new InterviewerFinishedInterview();
            finished.setCandidate_id(candidate_id);
            finished.setTimestamp(new DateTime());
            interviewer.getFinished_interviews().add(finished);
        }
        httpResponse.setStatus(RESTResponseEnum.REQUEST_OK.getStatus().value());
        return RESTResponseUtils.getRESTResponseAsString(RESTResponseEnum.REQUEST_OK);
    }

    private boolean isCandidateProcessedBy(final Candidate candidate, final String id) {
        boolean response = false;
        final Interviewer interviewer = interviewerRepository.findOne(id);
        if (interviewer.get_id() != null && !interviewer.get_id().isEmpty() && interviewer
                .getFunction() == EndavaFunction.HR) {
            if (!isPending(candidate)) {
                return true;
            }
        }
        if (candidate.getCurrent_application() == null) {
            return response;
        }
        for (final Stage stage : candidate.getCurrent_application().getStages()) {
            if (stage.getEmployee_id().equals(id)) {
                response = true;
                break;
            }
        }
        return response;
    }

    private boolean isFinalizedCandidate(final Candidate candidate, final String id) {
        return isCandidateProcessedBy(candidate, id) && !candidate.getCurrent_application().getInterview_result()
                .toString().toUpperCase().equals(InterviewResult.PENDING.toString());
    }

    private boolean isPending(final Candidate candidate) {
        return (candidate.getCurrent_application() != null && candidate.getCurrent_application()
                .getInterview_result() == InterviewResult.PENDING && candidate.getCurrent_application().getStages()
                        .size() == 0);
    }
}
