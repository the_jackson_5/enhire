package enhire.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import enhire.controllers.model.RESTResponseEnum;
import enhire.controllers.model.requests.InterviewerFeedbackRequest;
import enhire.controllers.model.responses.CandidateToInterviewerFeedback;
import enhire.controllers.model.responses.InterviewerFeedbackResponse;
import enhire.controllers.model.responses.InterviewerMetricsResponse;
import enhire.database.entities.*;
import enhire.database.entities.collections.Candidate;
import enhire.database.entities.collections.Interviewer;
import enhire.database.repositories.CandidateRepository;
import enhire.database.repositories.InterviewerRepository;
import enhire.utils.RESTResponseUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by slucutar on 4/16/2016.
 */
@RestController
public class InterviewerController {

    @Autowired
    private CandidateRepository candidateRepository;

    @Autowired
    private InterviewerRepository interviewerRepository;

    @RequestMapping(value = "/interviewers/{id}", method = RequestMethod.GET)
    public String getInterviewerInformation(@PathVariable(value = "id") final String id,
                                            final HttpServletResponse httpResponse) {
        final Interviewer interviewer = interviewerRepository.findOne(id);

        if (interviewer != null) {
            httpResponse.setStatus(HttpStatus.OK.value());
            return RESTResponseUtils.getRESTResponseAsString(interviewer);
        } else {
            httpResponse.setStatus(RESTResponseEnum.INTERVIEWER_NOT_FOUND.getStatus().value());
            return RESTResponseUtils.getRESTResponseAsString(RESTResponseEnum.INTERVIEWER_NOT_FOUND);
        }
    }

    @RequestMapping(value = "/interviewers/{id}/feedback", method = RequestMethod.GET)
    public String getAllCandidateFeedback(@PathVariable(value = "id") final String id,
                                          final HttpServletResponse httpResponse) {
        final Interviewer interviewer = interviewerRepository.findOne(id);
        if (interviewer == null) {
            httpResponse.setStatus(RESTResponseEnum.INTERVIEWER_NOT_FOUND.getStatus().value());
            return RESTResponseUtils.getRESTResponseAsString(RESTResponseEnum.INTERVIEWER_NOT_FOUND);
        }
        final InterviewerFeedbackResponse response = new InterviewerFeedbackResponse();
        for (final InterviewerFinishedInterview interview : interviewer.getFinished_interviews()) {
            System.out.println(interview.toString());
            if (interview.getCandidate_id() != null) {
                final CandidateToInterviewerFeedback feedback = new CandidateToInterviewerFeedback();
                final Candidate candidate = candidateRepository.findOne(interview.getCandidate_id());
                if (candidate != null) {
                    feedback.setCadidate_first_name(candidate.getFirst_name());
                    feedback.setCadidate_last_name(candidate.getLast_name());
                    feedback.setCadidate_feedback(interview.getCandidate_feedback());
                    feedback.setScore(interview.getScore());
                    response.add(feedback);
                }
            }
        }
        httpResponse.setStatus(HttpStatus.OK.value());
        return RESTResponseUtils.getRESTResponseAsString(response);
    }

    @RequestMapping(value = "/interviewers/{id}/feedback", method = RequestMethod.POST)
    public String candidateFeedback(@RequestBody final String body, @PathVariable(value = "id") final String id,
                                    final HttpServletResponse httpResponse) {
        final ObjectMapper om = new ObjectMapper();
        InterviewerFeedbackRequest request = null;
        try {
            request = om.readValue(body, InterviewerFeedbackRequest.class);
        } catch (final IOException e) {
            httpResponse.setStatus(RESTResponseEnum.INTERNAL_SERVER_ERROR.getStatus().value());
            return RESTResponseUtils.getRESTResponseAsString(RESTResponseEnum.INTERNAL_SERVER_ERROR);
        }

        if (request.getFeedback().isEmpty() || id.isEmpty() || request.getCandidate_id().isEmpty()) {
            httpResponse.setStatus(RESTResponseEnum.BAD_REQUEST.getStatus().value());
            return RESTResponseUtils.getRESTResponseAsString(RESTResponseEnum.BAD_REQUEST);
        }

        final Interviewer employee = interviewerRepository.findOne(id);
        if (employee == null) {
            httpResponse.setStatus(RESTResponseEnum.INTERVIEWER_NOT_FOUND.getStatus().value());
            return RESTResponseUtils.getRESTResponseAsString(RESTResponseEnum.INTERVIEWER_NOT_FOUND);
        }

        boolean foundInterview = false;
        for (final InterviewerFinishedInterview interview : employee.getFinished_interviews()) {
            if (interview.getCandidate_id().matches(request.getCandidate_id())) {
                interview.setCandidate_feedback(request.getFeedback());
                interview.setScore(request.getScore());
                foundInterview = true;
                break;
            }
        }

        if (foundInterview) {
            httpResponse.setStatus(RESTResponseEnum.REQUEST_OK.getStatus().value());
            return RESTResponseUtils.getRESTResponseAsString(RESTResponseEnum.REQUEST_OK);
        } else {
            final RESTResponseEnum response = RESTResponseEnum.INTERVIEWER_DID_NOT_PARTICIPATE;
            response.setMessage(response.getMessage().getMessage());
            httpResponse.setStatus(response.getStatus().value());
            return RESTResponseUtils.getRESTResponseAsString(response);
        }
    }

    @RequestMapping(value = "/interviewers", method = RequestMethod.GET)
    public String smartSearch(@RequestParam(value = "search_string") final String searchString, @RequestParam(
            value = "position") final String position, final HttpServletResponse httpResponse) {
        if (searchString == null || searchString.isEmpty()) {
            httpResponse.setStatus(RESTResponseEnum.INTERVIEWER_NOT_FOUND.getStatus().value());
            return RESTResponseUtils.getRESTResponseAsString(RESTResponseEnum.INTERVIEWER_NOT_FOUND);
        }

        final EndavaFunction function = EndavaFunction.fromString(position);
        if (function == null) {
            final RESTResponseEnum response = RESTResponseEnum.BAD_REQUEST;
            response.setMessage("Position is not valid");
            httpResponse.setStatus(RESTResponseEnum.BAD_REQUEST.getStatus().value());
            return RESTResponseUtils.getRESTResponseAsString(RESTResponseEnum.BAD_REQUEST);
        }

        final List<Interviewer> interviewers = interviewerRepository.findAll();
        final List<Interviewer> filteredInterviewers = new ArrayList<>();
        for (final Interviewer i : interviewers) {
            if (i.getFunction().equals(function)) {
                if ((i.getFirst_name() + i.getLast_name() + i.getEmail().toLowerCase()).contains(searchString
                        .toLowerCase())) {
                    filteredInterviewers.add(i);
                }
            }
        }
        if (filteredInterviewers.isEmpty()) {
            httpResponse.setStatus(RESTResponseEnum.INTERVIEWER_NOT_FOUND.getStatus().value());
            return RESTResponseUtils.getRESTResponseAsString(RESTResponseEnum.INTERVIEWER_NOT_FOUND);
        } else {
            httpResponse.setStatus(RESTResponseEnum.REQUEST_OK.getStatus().value());
            return RESTResponseUtils.getRESTResponseAsString(filteredInterviewers);
        }
    }

    @RequestMapping(value = "/interviewers/{id}/metrics", method = RequestMethod.GET)
    public String interviewerMetrics(@PathVariable(value = "id") final String id,
                                     final HttpServletResponse httpResponse) {
        final InterviewerMetricsResponse metricsResponse = new InterviewerMetricsResponse();

        final Interviewer interviewer = interviewerRepository.findOne(id);
        final List<Candidate> candidates = candidateRepository.findAll();
        metricsResponse.setAssigned_candidates(interviewer.getPending_interviews().size() + interviewer.getFinished_interviews().size());

        int accepted = 0;
        int rejected = 0;
        for (final InterviewerFinishedInterview interview : interviewer.getFinished_interviews()) {
            final Candidate candidate = candidateRepository.findOne(interview.getCandidate_id());
            for (final Interview interview1 : candidate.getPrevious_applications()) {
                for (final Stage stg : interview1.getStages()) {
                    if (stg.getEmployee_id().equals(interviewer.get_id())) {
                        final InterviewResult res = interview1.getInterview_result();
                        if (res == InterviewResult.ACCEPTED) accepted++;
                        else if (res == InterviewResult.REJECTED) rejected++;
                    }
                }
            }
        }
        metricsResponse.setAccepted_candidates(accepted);
        metricsResponse.setRejected_candidates(rejected);
        httpResponse.setStatus(RESTResponseEnum.REQUEST_OK.getStatus().value());
        return RESTResponseUtils.getRESTResponseAsString(metricsResponse);
    }
}
