package enhire.controllers;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * Created by slucutar on 4/16/2016.
 */
@RestController
public class TestController {

    @RequestMapping(value = "/health", method = RequestMethod.GET)
    public @ResponseBody Map<String, Object> health(final HttpServletResponse httpResponse) {
        httpResponse.setStatus(200);

        final Map<String, Object> response = new HashMap<>();
        response.put("status", "ok");
        return response;
    }

}
