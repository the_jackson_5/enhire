package enhire.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import enhire.controllers.model.RESTResponseEnum;
import enhire.controllers.model.requests.InterviewerLoginRequest;
import enhire.database.entities.collections.Interviewer;
import enhire.database.repositories.InterviewerRepository;
import enhire.utils.RESTResponseUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Created by slucutar on 4/16/2016.
 */
@RestController
public class LoginController {

    @Autowired
    private InterviewerRepository interviewerRepository;

    @RequestMapping(path = "/**", method = RequestMethod.OPTIONS)
    public String preflight() {
        return "";
    }

    @RequestMapping(path = "/login", method = RequestMethod.POST, headers = "Content-Type=application/json")
    public @ResponseBody String login(@RequestBody final String body, final HttpServletResponse httpResponse) {

        final ObjectMapper om = new ObjectMapper();
        InterviewerLoginRequest request = null;
        try {
            request = om.readValue(body, InterviewerLoginRequest.class);
        } catch (final IOException e) {
            httpResponse.setStatus(RESTResponseEnum.INTERNAL_SERVER_ERROR.getStatus().value());
            return RESTResponseUtils.getRESTResponseAsString(RESTResponseEnum.INTERNAL_SERVER_ERROR);
        }

        if (request.hasNullOrEmpty()) {
            httpResponse.setStatus(RESTResponseEnum.BAD_REQUEST.getStatus().value());
            return RESTResponseUtils.getRESTResponseAsString(RESTResponseEnum.BAD_REQUEST);
        }

        final List<Interviewer> interviewers = interviewerRepository.findByEmail(request.getEmail());

        if (interviewers.size() > 1) {
            httpResponse.setStatus(RESTResponseEnum.MULTIPLE_INTERVIEWERS_MATCH.getStatus().value());
            return RESTResponseUtils.getRESTResponseAsString(RESTResponseEnum.MULTIPLE_INTERVIEWERS_MATCH);
        }
        if (interviewers.size() == 0 || interviewers.get(0) == null) {
            httpResponse.setStatus(RESTResponseEnum.USER_UNAUTHORIZED.getStatus().value());
            return RESTResponseUtils.getRESTResponseAsString(RESTResponseEnum.USER_UNAUTHORIZED);
        }

        // Check password
        if (BCrypt.checkpw(request.getPassword(), interviewers.get(0).getPassword())) {
            httpResponse.setStatus(RESTResponseEnum.REQUEST_OK.getStatus().value());
            return "{\"id\":\"" + interviewers.get(0).get_id() + "\"}";
        } else {
            httpResponse.setStatus(RESTResponseEnum.USER_UNAUTHORIZED.getStatus().value());
            return RESTResponseUtils.getRESTResponseAsString(RESTResponseEnum.USER_UNAUTHORIZED);
        }
    }
}
