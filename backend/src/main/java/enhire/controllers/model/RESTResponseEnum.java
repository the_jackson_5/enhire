package enhire.controllers.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonValue;
import org.springframework.http.HttpStatus;

@JsonIgnoreProperties(ignoreUnknown = true)
public enum RESTResponseEnum {
    CANDIDATE_ALREADY_EXISTS(HttpStatus.CONFLICT, 100, "Candidate already exists"),
    INTERVIEWER_ALREADY_EXISTS(HttpStatus.CONFLICT, 100, "Interviewer already exists"),
    MULTIPLE_INTERVIEWERS_MATCH(HttpStatus.CONFLICT, 100, "Found multiple interviewers with same email. Fix it, it's unallowed !"),
    USER_UNAUTHORIZED(HttpStatus.UNAUTHORIZED, 102, "User unauthorized"),
    INVALID_EMAIL(HttpStatus.NOT_ACCEPTABLE, 102, "Email is invalid"),
    CANDIDATE_NOT_FOUND(HttpStatus.NOT_FOUND, 103, "Candidate not found"),
    CANDIDATE_REGISTER_SUCCESS(HttpStatus.CREATED, 200, "Candidate successfully registered"),
    CANDIDATE_DID_NOT_PARTICIPATE(HttpStatus.NOT_FOUND, 103, "Candidate did not participate at the interview"),
    CANDIDATE_NOT_ENGAGED_CURRENTLY_IN_INTERVIEW(HttpStatus.FAILED_DEPENDENCY, 104, "Candidate not engaged in an interview process"),
    INTERVIEWER_NOT_FOUND(HttpStatus.NOT_FOUND, 103, "Interviewer not found"),
    INTERVIEWER_DID_NOT_PARTICIPATE(HttpStatus.NOT_FOUND, 103, "Interviewer did not interview candidate"),
    REQUEST_OK(HttpStatus.OK, 200, "Request successful, changes are made and we don't want to return more data"),
    BAD_REQUEST(HttpStatus.BAD_REQUEST, 400, "Bad request"),
    INTERNAL_SERVER_ERROR(HttpStatus.INTERNAL_SERVER_ERROR, 500, "Internal server error"),
    EMAIL_ERROR(HttpStatus.INTERNAL_SERVER_ERROR, 500, "Email could not be sent");

    private final Integer code;
    private String message;
    private final HttpStatus status;

    RESTResponseEnum(final HttpStatus status, final Integer code, final String message) {
        this.code = code;
        this.message = message;
        this.status = status;
    }

    public HttpStatus getStatus() {
        return status;
    }

    @JsonValue
    public RESTResponse getMessage() {
        return new RESTResponse(code, message);
    }

    public void setMessage(final String message) {
        this.message = message;
    }
}
