package enhire.controllers.model.requests;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by slucutar on 4/16/2016.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class InterviewerFeedbackRequest {
    private String candidate_id;
    private String feedback;
    private Integer score;

    public String getCandidate_id() {
        return candidate_id;
    }

    public void setCandidate_id(final String candidate_id) {
        this.candidate_id = candidate_id;
    }

    public String getFeedback() {
        return feedback;
    }

    public void setFeedback(final String feedback) {
        this.feedback = feedback;
    }

    public Integer getScore() {
        return score;
    }

    public void setScore(final Integer score) {
        this.score = score;
    }
}
