package enhire.controllers.model.requests;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class FinalizeInterviewRequest {
    private String final_answer;
    private String interviewer_id;

    public String getFinal_answer() {
        return final_answer;
    }

    public void setFinal_answer(final String final_answer) {
        this.final_answer = final_answer;
    }

    public String getInterviewer_id() {
        return interviewer_id;
    }

    public void setInterviewer_id(final String interviewer_id) {
        this.interviewer_id = interviewer_id;
    }
}
