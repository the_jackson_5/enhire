package enhire.controllers.model.requests;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import enhire.database.entities.GradeEnum;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CandidateFeedbackForInterviewerRequest {
    private String interviewer_id;
    private String grade;

    public String getInterviewer_id() {
        return interviewer_id;
    }

    public void setCandidate_id(final String interviewer_id) {
        this.interviewer_id = interviewer_id;
    }

    public String getGrade() {
        return grade;
    }

    public void setGrade(final String grade) {
        this.grade = grade;
    }

    public boolean hasNullOrEmpty() {
        return (interviewer_id == null || interviewer_id.isEmpty() || grade == null || grade.isEmpty());
    }
}
