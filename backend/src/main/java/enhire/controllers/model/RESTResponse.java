package enhire.controllers.model;

/**
 * Created by slucutar on 4/16/2016.
 */
public class RESTResponse {

    private Integer code;
    private String message;

    public RESTResponse(final Integer code, final String message) {
        this.code = code;
        this.message = message;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(final Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(final String message) {
        this.message = message;
    }
}
