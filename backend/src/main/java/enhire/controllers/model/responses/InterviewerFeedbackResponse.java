package enhire.controllers.model.responses;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by slucutar on 4/16/2016.
 */
public class InterviewerFeedbackResponse {

    private final List<CandidateToInterviewerFeedback> feedbacks;

    public InterviewerFeedbackResponse() {
        feedbacks = new ArrayList<>();
    }

    public void add(final CandidateToInterviewerFeedback feedback) {
        feedbacks.add(feedback);
    }

    public List<CandidateToInterviewerFeedback> getFeedbacks() {
        return feedbacks;
    }
}
