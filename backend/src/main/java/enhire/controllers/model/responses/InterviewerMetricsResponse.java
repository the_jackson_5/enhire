package enhire.controllers.model.responses;

/**
 * Created by slucutar on 4/17/2016.
 */
public class InterviewerMetricsResponse {

    private Integer assigned_candidates;
    private Integer accepted_candidates;
    private Integer rejected_candidates;

    public Integer getAssigned_candidates() {
        return assigned_candidates;
    }

    public void setAssigned_candidates(final Integer assigned_candidates) {
        this.assigned_candidates = assigned_candidates;
    }

    public Integer getAccepted_candidates() {
        return accepted_candidates;
    }

    public void setAccepted_candidates(final Integer accepted_candidates) {
        this.accepted_candidates = accepted_candidates;
    }

    public Integer getRejected_candidates() {
        return rejected_candidates;
    }

    public void setRejected_candidates(final Integer rejected_candidates) {
        this.rejected_candidates = rejected_candidates;
    }
}
