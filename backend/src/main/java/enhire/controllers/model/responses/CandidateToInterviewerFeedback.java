package enhire.controllers.model.responses;

/**
 * Created by slucutar on 4/16/2016.
 */
public class CandidateToInterviewerFeedback {

    public String cadidate_first_name;
    public String cadidate_last_name;
    public String cadidate_feedback;
    public Integer score;

    public String getCadidate_first_name() {
        return cadidate_first_name;
    }

    public void setCadidate_first_name(final String cadidate_first_name) {
        this.cadidate_first_name = cadidate_first_name;
    }

    public String getCadidate_last_name() {
        return cadidate_last_name;
    }

    public void setCadidate_last_name(final String cadidate_last_name) {
        this.cadidate_last_name = cadidate_last_name;
    }

    public String getCadidate_feedback() {
        return cadidate_feedback;
    }

    public void setCadidate_feedback(final String cadidate_feedback) {
        this.cadidate_feedback = cadidate_feedback;
    }

    public Integer getScore() {
        return score;
    }

    public void setScore(final Integer score) {
        this.score = score;
    }
}
