package enhire.data_structures;

// Java does not have tuples...
public class Tuple<X, Y> {
    X first;
    Y second;

    public Tuple() {
    }

    public Tuple(final X x, final Y y) {
        this.first = x;
        this.second = y;
    }

    public X getFirst() {
        return first;
    }

    public Y getSecond() {
        return second;
    }

    public void setFirst(final X first) {
        this.first = first;
    }

    public void setSecond(final Y second) {
        this.second = second;
    }
}
