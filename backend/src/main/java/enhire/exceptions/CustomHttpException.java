package enhire.exceptions;

import java.util.HashMap;
import java.util.Map;
import java.util.StringJoiner;

/**
 * Created by slucutar on 4/16/2016.
 */
public class CustomHttpException {

    public static Map<String, Object> create(final int code, final String message) {
        final Map<String, Object> response = new HashMap<>();
        final Map<String, Object> error = new HashMap<>();
        error.put("code", code);
        error.put("message", message);
        response.put("error", error);
        return response;
    }
}
