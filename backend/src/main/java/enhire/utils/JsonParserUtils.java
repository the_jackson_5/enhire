package enhire.utils;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;

/**
 * Created by slucutar on 4/16/2016.
 */
public class JsonParserUtils {
    public static JsonNode parseString(final String body) {
        final ObjectMapper mapper = new ObjectMapper();
        JsonNode json = null;
        try {
            json = mapper.readTree(body);

        } catch (final IOException e) {
            e.printStackTrace();
        }
        return json;
    }
}
