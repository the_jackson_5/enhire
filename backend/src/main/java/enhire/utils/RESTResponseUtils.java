package enhire.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import enhire.controllers.model.RESTResponseEnum;

/**
 * Created by slucutar on 4/16/2016.
 */
public class RESTResponseUtils {

    public static String getRESTResponseAsString(final Object response) {
        final ObjectMapper om = new ObjectMapper();

        try {
            return om.writeValueAsString(response);
        } catch (final JsonProcessingException e) {
            return "{\"code\":500,\"message\":\"Server cannot process your request\"}";
        }
    }
}
