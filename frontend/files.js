/* dependencies */
var path = require('path'),
  glob = require('glob'),
  projectRoot = path.join(__dirname + '/');

/* folder variables */
var testFolders,
  jsFolders,
  libFolders,
  files;

function pathResolver(currentPath) {
  return path.resolve(currentPath);
}

/* folder path resolvers */
testFolders = glob.sync(projectRoot + 'app/**/tests').map(pathResolver);
jsFolders = glob.sync(projectRoot + 'app/**/js').map(pathResolver);
bddFolders = glob.sync(projectRoot + 'app/**/bdd').map(pathResolver);

files = {
  projectRoot: projectRoot,
  sourceFolder: 'app',
  sourceFolderPath: projectRoot + 'app/',
  buildFolder: 'build',
  buildFolderPath: projectRoot + 'build/',
  js: {
    all: 'app/**/js/*.js',
    bundle: 'main.bundle.js',
    main: 'main.js'
  },
  mockServer: {
    serverScript: 'mock-server/mock-server.js',
    database: projectRoot + 'mock-database/database.json',
    databaseFolder: projectRoot + 'mock-database/',
    mocksFiles: projectRoot + 'app/**/mocks/*.json'
  }
};

global.files = files;
