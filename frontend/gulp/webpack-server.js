var webpack = require('webpack'),
	webpackDevStream = require('webpack-dev-server'),
  webpackConfig = require('../webpack.conf.js'),
  webpackServerConfig = require('./configs/webpack-server.js'),
	objectAssign = require('object-assign'),
  path = require('path');

module.exports = function(gulp, plugins) {
  return function() {
		webpackConfig = objectAssign({}, webpackConfig, {
      devtool: 'eval-cheap-module-source-map',
      debug: true
    });

    webpackConfig.entry.app.unshift('webpack/hot/dev-server');
    webpackConfig.entry.app.unshift('webpack-dev-server/client?http://localhost:3000');

    new webpackDevStream(webpack(webpackConfig), webpackServerConfig)
    .listen(3000, 'localhost', function(err) {
      if(err){
        throw new gutil.PluginError("webpack-dev-server", err);
      }
    });
  };
};
