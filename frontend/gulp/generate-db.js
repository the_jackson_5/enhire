module.exports = function(gulp, plugins) {
  return function() {
    return gulp.src(global.files.mockServer.mocksFiles)
      .pipe(plugins.extend('database.json', true))
      .pipe(gulp.dest(global.files.mockServer.databaseFolder))
  }
}
