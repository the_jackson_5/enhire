var utils = require('./utils');

module.exports = function (gulp, plugins) {
  return function() {
    return plugins.nodemon({
          verbose: false,
          script: global.files.mockServer.serverScript,
          ext:'json'
        });
  }
};
