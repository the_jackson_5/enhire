var webpack = require('webpack'),
  webpackConfig = require('../webpack.conf.js'),
  webpackServerConfig = require('./configs/webpack-server.js');

module.exports = function(gulp, plugins) {
  return function(done) {

    if(plugins.util.env.ENV === 'development') {
      webpackConfig.devtool = 'inline-source-map';
    }

    webpack(webpackConfig, function(err, stats) {
      if(err) throw new plugins.util.PluginError("webpack", err);
      else {
        plugins.util.log(stats.toString(webpackServerConfig.stats));
      }
      done();
    });
  };
};
