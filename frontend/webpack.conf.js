var webpackConfig,
  webpack = require('webpack'),
  HtmlWebpackPlugin = require('html-webpack-plugin'),
  ExtractTextPlugin = require("extract-text-webpack-plugin"),
  extractCSS = new ExtractTextPlugin('[name].css'),
  plugins = require('gulp-load-plugins')();

webpackConfig = {
  context: global.files.projectRoot,
  entry: {
    app: [
      global.files.sourceFolderPath  + global.files.js.main
    ],
    vendor: ['react', 'react-dom', 'material-ui', 'formsy-material-ui', 'react-router', 'history', 'formsy-react', 'baobab-react', 'stampit', 'restful.js']
  },
  output: {
    path: global.files.buildFolderPath,
    publicPath: '/',
    filename: global.files.js.bundle,
    chunkFilename: '[id].' + global.files.js.bundle
  },
  module: {
    preLoaders: [
      {
        test: /\.js$/,
        exclude: /(node_modules|bower_components|build|gulp)/,
        loader: 'eslint-loader'
      }
    ],
    loaders: [
      {
        test: /\.js?$/,
        exclude: /(node_modules|bower_components|build|gulp)/,
        loaders: ['react-hot']
      },
      {
        test: /\.scss$/i,
        exclude: /(node_modules|bower_components|build|gulp)/,
        loader: extractCSS.extract(['css', 'resolve-url', 'sass?sourceMap'])
      },
      {
        test: /\.(jpe?g|png|gif|svg)$/i,
        loaders: [
          'file?hash=sha512&digest=hex&name=[hash].[ext]',
          'image-webpack?bypassOnDebug&optimizationLevel=7&interlaced=false'
        ]
      },
      {
        test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
        loader: "url-loader?limit=10000&mimetype=application/font-woff"
      },
      {
        test: /\.(ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
        loader: "file-loader"
      },
      {
        test: /\.json?$/,
        loader: "json-loader"
      },
      {
        test: /\.js?$/,
        exclude: /(node_modules|bower_components|build|gulp)/,
        loader: 'babel?presets[]=react,presets[]=es2015,presets[]=stage-0&plugins[]=transform-runtime&cacheDirectory'
      }
    ]
  },
  eslint: {
    configFile: global.files.projectRoot + '.eslintrc',
    emitError: true,
    failOnError: true
  },
  resolve: {
    extensions: ['', '.js', '.jsx', '.scss'],
    root: [global.files.projectRoot],
    modulesDirectories: [
      'node_modules'
    ]
  },
  plugins: [
    extractCSS,
    new webpack.HotModuleReplacementPlugin(),
    new HtmlWebpackPlugin({
      title: 'Enhire',
      hash: true,
      template: './app/main.html'
    }),
    new webpack.optimize.CommonsChunkPlugin({
      name : 'vendor',
      filename: "vendors.js",
      minChunks: Infinity
    }),
    new webpack.ProvidePlugin({
      Promise: 'imports?this=>global!exports?global.Promise!es6-promise',
      fetch: 'imports?this=>global!exports?global.fetch!whatwg-fetch',
    }),
    new webpack.DefinePlugin({
      CONF: require(global.files.sourceFolderPath + 'common/js/config_' + plugins.util.env.ENV + '.js')
    }),
    new webpack.optimize.UglifyJsPlugin({
      compress: {
        warnings: false
      },
      mangle: {
        dedupe: true,
        minimize: true,
        except: ['exports', 'require']
      }
    })
  ]
};

module.exports = webpackConfig;
