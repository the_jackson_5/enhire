import { Router } from 'react-router';
import React from 'react';
import ReactDOM from 'react-dom';
import rootRoute from './pages/app/js/route';
import browserHistory from 'app/common/components/history/js/history';
import injectTapEventPlugin from 'react-tap-event-plugin';

injectTapEventPlugin();

ReactDOM.render(
  <Router
      history={browserHistory}
      routes={rootRoute}
  />, document.getElementById('app'));
