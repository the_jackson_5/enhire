import React from 'react';
import Paper from 'material-ui/lib/paper';
import Formsy from 'formsy-react';
import FMUI from 'formsy-material-ui';
import StarRating from '../components/star-rating/js/star-rating';
import CardTitle from 'material-ui/lib/card/card-title';
import CardText from 'material-ui/lib/card/card-text';
import RaisedButton from 'material-ui/lib/raised-button';
import CircularProgress from 'material-ui/lib/circular-progress';
import Snackbar from 'material-ui/lib/snackbar';

import CandidateFeedbackState from './candidate-feedback-state';
import {branch} from 'baobab-react/higher-order';

import CandidateModel from 'app/common/components/candidate-model/js/candidate-model';
import candidateDef from 'app/common/components/candidate-model/js/candidate-model-def';
import InterviewerModel from 'app/common/components/interviewer-model/js/interviewer-model';
import interviewerDef from 'app/common/components/interviewer-model/js/interviewer-model-def';

var CandidateFeedback = {},
  candidateModelInstance,
  interviewerModelInstance,
  candidateFeedbackInstance;

CandidateFeedback.Class = React.createClass({
  displayName: "Candidate Feedback Page",
  propTypes: {
    candidate: React.PropTypes.object,
    interviewer: React.PropTypes.object,
    location: React.PropTypes.object,
  },
  getInitialState() {
    return {
      canSubmit: false,
      hasData: false,
      showMessage: false,
      message: 'Feedback Successfully Sent'
    };
  },
  componentWillMount() {
    let { query } = this.props.location;

    this.hasIDs = true;

    if(!query){
      this.hasIDs = false;
      return;
    }

    this.candidateId = query.candidate_id;
    this.interviewerId = query.interviewer_id;
    this.interviewId = query.interview_id;

    if(!this.candidateId || !this.interviewerId) {
      this.hasIDs = false;
      return;
    }

    Promise.all([this.getInterviewerData(), this.getCandidateData()]).then(() => {
      candidateFeedbackInstance = CandidateFeedbackState(interviewerModelInstance);
      this.setState({
        hasData: true,
        candidate: candidateModelInstance.cursor.get(),
        interviewer: interviewerModelInstance.cursor.get()
      });

    },() => {
      this.setState({
        hasData: true,
        candidate: null,
        interviewer: null
      });

    });
  },
  enableSubmit() {
    this.setState({canSubmit: true});
  },
  disableSubmit() {
    this.setState({canSubmit: false});
  },
  notifyError (model, reset, setErrorState) {
      setErrorState('Form Submission Failed');
  },
  setRating(rating) {
      this.refs.ratingInput.setValue(rating);
  },
  handleRequestClose() {
    this.setState({
      open: false,
    });
  },
  submitFeedback(model) {
    let data = {
      feedback: model.feedback,
      score: model.score || 1,
      candidate_id: this.candidateId
    },
    self = this;

    candidateFeedbackInstance.postData(data, self.feedbackCallback);
  },
  feedbackCallback(self, data) {
    if(data.error) {
      this.feedbackRequestError();
      return;
    }

    this.feedbackRequestSuccess();
  },
  feedbackRequestSuccess(){
    this.refs.feedbackForm.reset();
    this.setState({
      showMessage: true,
      message: "Feedback submited successfuly"
    });
  },
  feedbackRequestError(){
    this.refs.feedbackForm.reset();
    this.setState({
      showMessage: true,
      message: "An error occured when submiting your feedback."
    });
  },
  initCandidateModel() {
    candidateDef.path = ['candidate-feedback', 'candidate'];
    candidateDef.id = this.candidateId;
    candidateModelInstance = CandidateModel(candidateDef);
  },
  initInterviewerModel() {
    interviewerDef.path = ['candidate-feedback', 'interviewer'];
    interviewerDef.id = this.interviewerId;
    interviewerModelInstance = InterviewerModel(interviewerDef);

  },
  getInterviewerData() {
    this.initInterviewerModel();

    return interviewerModelInstance.getData();
  },
  getCandidateData() {
    this.initCandidateModel();

    return candidateModelInstance.getData();
  },
  renderFeedbackPage() {
    console.log(this.state);
    if (!this.state.candidate || !this.state.interviewer) {
        return this.renderErrorComponent();
    }

    return (
      <section className="candidate-feedback-page">
        <Paper
            style={{
              overflow: "hidden",
              padding: "20px",
              minWidth: "100%",
              width: "100%"
            }}
        >
          <CardTitle
              style={{
                padding: "2rem 0"
              }}
              title={"Hello " + this.state.candidate.first_name}
          />
          <p>
            {"Welcome to feedback survey for Endava Interviews. "}
            {"Here you can post your feedback regarding you last interview with " + this.state.interviewer.first_name + ". "}
            {"Please leave a rating for the interview by clicking on the gold stars."}
          </p>
          <Formsy.Form
              onInvalid={this.disableSubmit}
              onInvalidSubmit={this.notifyError}
              onValid={this.enableSubmit}
              onValidSubmit={this.submitFeedback}
              ref="feedbackForm"
          >
            <FMUI.FormsyText
                floatingLabelText="Your Feedback"
                multiLine
                name="feedback"
                required
                style={{
                  width: "100%"
                }}
                validationError="Please fill In this input before submiting"
                validations="isExisty"
            />
            <FMUI.FormsyText
                name="score"
                ref="ratingInput"
                style={{
                  display:"none"
                }}
            />
            <StarRating.Class
                onSelect={this.setRating}
            />
            <RaisedButton
                className="feedback-btn"
                disabled={!this.state.canSubmit}
                label="Send Feedback"
                required
                secondary
                style={{
                  display: "block",
                  float: "right",
                  width: "50%"
                }}
                type="submit"
            />
          </Formsy.Form>
        </Paper>
        <Snackbar
            autoHideDuration={4000}
            message={this.state.message}
            onRequestClose={this.handleRequestClose}
            open={this.state.showMessage}
        />
      </section>
    );
  },
  renderErrorComponent() {
    return (
      <section className="candidate-feedback-page">
        <Paper
            style={{
              overflow: "hidden",
              padding: "20px",
              width: "100%"
            }}
        >
          <CardTitle
              style={{
                padding: "2rem 0"
              }}
              subtitle="Unable to generate feedback page"
              title="Render Error"
          />
          <CardText>
             Sorry about that but we could not retrieve the relevant data to generate the feedback page.
          </CardText>
        </Paper>
        <Snackbar
            autoHideDuration={4000}
            message={this.state.message}
            onRequestClose={this.handleRequestClose}
            open={this.state.showMessage}
        />
      </section>
    );
  },
  renderLoader() {
    return(
      <section className="candidate-feedback-page">
          <CircularProgress size={1} />
      </section>
    ) ;
  },
  render() {
    return this.state.hasData ? this.renderFeedbackPage() : this.renderLoader();
  }
});


export default CandidateFeedback;
