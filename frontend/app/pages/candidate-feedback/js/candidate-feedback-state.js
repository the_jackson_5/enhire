import CandidateFeedbackModel from './candidate-feedback-model';
import Baobab from 'baobab';

let feedbackOptions = {
  initialState: {
    $loaded: false,
    list: []
  },
  subCursors: {
    $loaded: ['$loaded'],
    list: ['list']
  }
};

function getFeedbackStateInstance(interviewerModel) {

  return CandidateFeedbackModel({
    route: 'feedback',
    rootEndpoint: interviewerModel._getEndpoint(),
    path: ['candidate-feedback'],
    type:'custom',
    options: feedbackOptions
  });
}


export default getFeedbackStateInstance;
