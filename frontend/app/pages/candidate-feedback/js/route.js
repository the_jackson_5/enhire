import CandidateFeedbackPage from './candidate-feedback';
import constants from 'app/common/components/constants/js/constants';

export default {
  path: constants.candidateFeedbackURL,
  getComponent(location, cb) {
    require.ensure([], () => {
      cb(null, CandidateFeedbackPage.Class);
    });
  }
};
