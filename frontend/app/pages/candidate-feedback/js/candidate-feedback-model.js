import Stampit from 'stampit';
import BaseModel from 'app/common/components/base-model/js/base-member';

var CandidateFeedbackModel = Stampit
  .methods({
    enableData() {
      this.cursor.set('$loaded', true);
    },
    saveDataToTree(data) {
      if(!data) {
        return;
      }

      this.cursor.set('$loaded', true);
      this.cursor.set('list', data.feedbacks);
    }
  });


export default BaseModel.compose(CandidateFeedbackModel);
