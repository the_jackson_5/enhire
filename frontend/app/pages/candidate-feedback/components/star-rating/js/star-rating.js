import React from 'react';

let StarRating = {};

StarRating.Class = React.createClass({
  displayName : "Star Rating Component",
  propTypes: {
    defaultValue: React.PropTypes.number,
    disabled: React.PropTypes.bool,
    onSelect: React.PropTypes.func
  },
  getInitialState() {
    let value = this.props.defaultValue || 1;

    return ({
      selected: value
    });
  },
  changeRating(event) {
    let rating = event.target.getAttribute('data-rating');

    if(this.props.onSelect && this.props.onSelect instanceof Function) {
      this.props.onSelect(event.target.getAttribute('data-rating'));
    }

    this.setState({selected: rating});
  },
  renderStars() {
    let stars = [], index;

    for (index = 1; index < 6; index++) {
      stars.push(
        <span
            className={this.state.selected >= index ? 'star selected' : 'star'}
            data-rating={index}
            key={index}
            onClick={this.changeRating}
        >☆
        </span>);
    }

    return stars.reverse();
  },
  render() {
    return (
      <div className={this.props.disabled ? 'rating' : 'rating hoverable'}>
      <label>Give A Rating</label>
        {this.renderStars()}
      </div>
    );
  }
});


export default StarRating;
