import React from 'react';
import {Link} from 'react-router';
import {root, branch} from 'baobab-react/higher-order';
import getFeedbackStateInstance from 'app/pages/candidate-feedback/js/candidate-feedback-state';
import AuthModel from 'app/common/components/auth/js/auth';
import interviewerModel from 'app/common/components/interviewer-model/js/interviewer-model';
import interviewerModelDef from 'app/common/components/interviewer-model/js/interviewer-model-def';

import Card from 'material-ui/lib/card/card';
import CardActions from 'material-ui/lib/card/card-actions';
import CardHeader from 'material-ui/lib/card/card-header';
import FlatButton from 'material-ui/lib/flat-button';
import CardText from 'material-ui/lib/card/card-text';

let Feedbacks = {};

Feedbacks.Class = React.createClass({
  displayName: 'Feedbacks',
  propTypes: {
    interviewer_id: React.PropTypes.number
  },
  getInitialState() {
    return {
      feedbacksList: []
    };
  },
  componentWillMount() {
    interviewerModelDef.id = this.props.interviewer_id;
    this.interviewerModelinstance = interviewerModel(interviewerModelDef);
    this.feedbacksModelInstance = getFeedbackStateInstance(this.interviewerModelinstance);
    this.feedbacksModelInstance.getData(this.getFeedbacksCb);
  },
  getFeedbacksCb(data) {
    this.setState({
      feedbacksList: data.feedbacks
    });
  },
  render() {
    return (
      <div className="page feedback-page">
        {this.state.feedbacksList.map((feedback, index) => {
          return (
            <Card key={index}>
              <CardHeader
                  actAsExpander
                  showExpandableButton
                  subtitle={'Rating: ' + feedback.score}
                  title={feedback.cadidate_first_name + ' ' + feedback.cadidate_first_name}
              />
              <CardText expandable>
                {feedback.cadidate_feedback}
              </CardText>
            </Card>
          );
        })}
      </div>
    );
  }
});

Feedbacks.Branch = branch(Feedbacks.Class, {
  cursors: AuthModel.getSubCursors()
});


export default Feedbacks;
