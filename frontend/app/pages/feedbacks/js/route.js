import Feedback from './feedback';
import constants from 'app/common/components/constants/js/constants';

export default {
  path: constants.feedbacksUrl,
  getComponent(location, cb) {
    require.ensure([], () => {
      cb(null, Feedback.Branch);
    });
  }
};
