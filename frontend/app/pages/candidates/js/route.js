import Candidates from './candidates';
import constants from 'app/common/components/constants/js/constants';

export default {
  path: constants.candidatesUrl,
  getComponent(location, cb) {
    require.ensure([], () => {
      cb(null, Candidates.Branch);
    });
  }
};
