import React from 'react';
import {Link} from 'react-router';
import {root, branch} from 'baobab-react/higher-order';
import CandidatesList from 'app/common/components/candidates-list/js/candidates-list';
import AuthModel from 'app/common/components/auth/js/auth';

let Candidates = {};

Candidates.Class = React.createClass({
  displayName: 'Candidates',
  propTypes: {
    interviewer_id: React.PropTypes.number
  },
  render() {
    return (
      <div className="page candidates-page">
        <CandidatesList.Branch status="pending" />
        <CandidatesList.Branch
            interviewer_id={this.props.interviewer_id}
            status="processed"
        />
      </div>
    );
  }
});

Candidates.Branch = branch(Candidates.Class, {
  cursors: AuthModel.getSubCursors()
});


export default Candidates;
