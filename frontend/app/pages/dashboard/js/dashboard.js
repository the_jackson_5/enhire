import React from 'react';
import {Link} from 'react-router';
import {root, branch} from 'baobab-react/higher-order';

import StatsCardList from '../components/stat-list/js/stats-list';

let Dashboard = {};

Dashboard.Class = React.createClass({
  displayName: 'Dashboard',
  render() {
    return (
      <div className="page dashboard-page">
        <StatsCardList.Class/>
      </div>
    );
  }
});

export default Dashboard;
