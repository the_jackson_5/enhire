import React from 'react';
import Paper from 'material-ui/lib/paper';
import getStatModelInstance from './stats-list-state';
import AuthModel from 'app/common/components/auth/js/auth';
import InterviewerModel from 'app/common/components/interviewer-model/js/interviewer-model';
import InterviewerModelDef from 'app/common/components/interviewer-model/js/interviewer-model-def';
import StatCard from '../../stats-card/js/stats-card';

let StatsCardList = {},
  statsModelInstance,
  interviewerModelInstance;

StatsCardList.Class = React.createClass({
  displayName: 'Stats Card',
  getInitialState(){
    return {
        assigned_candidates: "N/A",
        accepted_candidate: "N/A",
        rejected_candidates: "N/A"
    };
  },
  componentWillMount(){
    InterviewerModelDef.id = AuthModel.cursor.get('interviewer_id');
    interviewerModelInstance = InterviewerModel(InterviewerModelDef);

    statsModelInstance = getStatModelInstance(interviewerModelInstance);
    statsModelInstance.getData(this.statsCallback);
  },
  statsCallback(data){
    if(!data) {
      return;
    }

    this.setState({
      assigned_candidates: data.assigned_candidates || "N/A",
      accepted_candidate: data.accepted_candidate || "N/A",
      rejected_candidates: data.rejected_candidates || "N/A"
    });
  },
  render() {
    return (
        <div className="stat-cards-row">
          <StatCard.Class
              borderColor="blue"
              statistic= "Assigned Candidates"
              value= {this.state.assigned_candidates}
          />
          <StatCard.Class
              borderColor="green"
              statistic= "Accepted Candidates"
              value= {this.state.accepted_candidate}
          />
          <StatCard.Class
              borderColor="red"
              statistic= "Rejected Candidates"
              value= {this.state.rejected_candidates}
          />
        </div>
    );
  }
});

export default StatsCardList;
