import StatModel from './stats-list-model';
import Baobab from 'baobab';

let statOptions = {
  initialState: {},
  subCursors: {}
};

function getStatModelInstance(interviewerModel) {

  return StatModel({
    route: 'metrics',
    rootEndpoint: interviewerModel._getEndpoint(),
    path: ['metrics'],
    type:'custom',
    options: statOptions
  });
}


export default getStatModelInstance;
