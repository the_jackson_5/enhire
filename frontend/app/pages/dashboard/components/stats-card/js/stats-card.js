import React from 'react';
import Paper from 'material-ui/lib/paper';

let StatsCard = {};

StatsCard.Class = React.createClass({
  displayName: 'Stats Card',
  propTypes: {
    borderColor: React.PropTypes.string,
    statistic: React.PropTypes.string,
    value: React.PropTypes.oneOfType([
      React.PropTypes.string,
      React.PropTypes.number,
    ])
  },
  render() {
    return (
        <Paper
            className={"stats-card " + this.props.borderColor}
        >
        <span className="statistic">{this.props.statistic}</span>
        <span className="value">{this.props.value}</span>
        </Paper>
    );
  }
});

export default StatsCard;
