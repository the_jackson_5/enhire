import React from 'react';
import {root, branch} from 'baobab-react/higher-order';
import FMUI from 'formsy-material-ui';
import Formsy from 'formsy-react';
import Paper from 'material-ui/lib/paper';
import CardTitle from 'material-ui/lib/card/card-title';
import RaisedButton from 'material-ui/lib/raised-button';
import AuthModel from 'app/common/components/auth/js/auth';

let Login = {};

const { FormsyText } = FMUI;

Login.Class = React.createClass({
  displayName: 'Login',
  getInitialState: function () {
    return {
      canSubmit: false
    };
  },
  enableButton: function () {
    this.setState({
      canSubmit: true
    });
  },
  disableButton: function () {
    this.setState({
      canSubmit: false
    });
  },
  loginError() {
    if (this.state.showPassword) {
      this.refs.showPassword.setValue(false);
    }

    this.setState({
      formError: true,
      showPassword: false
    });

    this.disableButton();
  },
  loginCallback(self, data) {
    if(data.error) {
      this.loginError();
      return;
    }
  },
  submitForm(model) {
    AuthModel.postData(model, this.loginCallback);
  },
  render() {
    return (
      <section className="login-page">
        <Paper
            style={{
              overflow: "hidden",
              padding: "20px",
              width: "100%"
            }}
        >
          <Formsy.Form
              onInvalid={this.disableButton}
              onValid={this.enableButton}
              onValidSubmit={this.submitForm}
          >
            <CardTitle
                style={{
                  padding: 0
                }}
                subtitle="Please login using your Endava credentials"
                title="Welcome to ENhire"
            />
            <FormsyText
                floatingLabelText="Email"
                hintText="Email"
                name="email"
                required
                style={{
                  width: "100%"
                }}
                validationError="Please enter a valid email address"
                validations="isEmail"
                value=""
            />
            <FormsyText
                floatingLabelText="Password"
                hintText="Password"
                name="password"
                required
                style={{
                  width: "100%"
                }}
                type="password"
                value=""
            />
            <RaisedButton
                className="login-btn"
                disabled={!this.state.canSubmit}
                label="Login"
                required
                secondary
                style={{
                  float: "right",
                  marginTop: "2rem"
                }}
                type="submit"
            />
          </Formsy.Form>
        </Paper>
      </section>
    );
  }
});

export default Login;
