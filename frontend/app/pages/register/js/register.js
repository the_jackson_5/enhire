import React from 'react';
import {Link} from 'react-router';
import {root, branch} from 'baobab-react/higher-order';
import Paper from 'material-ui/lib/paper';
import CardTitle from 'material-ui/lib/card/card-title';
import FMUI from 'formsy-material-ui';
import Formsy from 'formsy-react';
import RaisedButton from 'material-ui/lib/raised-button';
import MenuItem from 'material-ui/lib/menus/menu-item';
import RegisterModel from './register-state';
import Snackbar from 'material-ui/lib/snackbar';

let Register = {};
const { FormsyText, FormsySelect } = FMUI;

Register.Class = React.createClass({
  displayName: 'Register',
  getInitialState: function () {
    return {
      canSubmit: false,
      open: false
    };
  },
  enableButton: function () {
    this.setState({
      canSubmit: true
    });
  },
  disableButton: function () {
    this.setState({
      canSubmit: false
    });
  },
  handleRequestClose() {
    this.setState({
      open: false,
    });
  },
  handleTouchTap() {
    this.setState({
      open: true,
    });
  },
  registerError() {
    this.disableButton();
  },
  registerSuccess() {
    this.refs.registerForm.reset();
    this.setState({
      open: true
    });
  },
  registerCallback(self, data) {
    if(data.error) {
      this.registerError();
      return;
    }

    this.registerSuccess();
  },
  submitForm(model) {
    RegisterModel.postData(model, this.registerCallback);
  },
  render() {
    return (
      <div className="page dashboard-page">
        <Paper
            className="register-form-wrapper"
            style={{
              overflow: "hidden",
              padding: "20px",
              width: "60%"
            }}
        >
          <Formsy.Form
              onInvalid={this.disableButton}
              onValid={this.enableButton}
              onValidSubmit={this.submitForm}
              ref="registerForm"
          >
            <CardTitle
                style={{
                  padding: 0
                }}
                title="Add candidate"
            />
            <FormsyText
                floatingLabelText="First Name"
                hintText="First Name"
                name="first_name"
                required
                style={{
                  float: "left",
                  marginRight: "5%",
                  width: "45%"
                }}
                value=""
            />
            <FormsyText
                floatingLabelText="Last Name"
                hintText="Last Name"
                name="last_name"
                required
                style={{
                  float: "right",
                  marginLeft: "5%",
                  width: "45%"
                }}
                value=""
            />
            <FormsyText
                floatingLabelText="Phone Number"
                hintText="Phone Number"
                name="phone"
                required
                style={{
                  float: "left",
                  marginRight: "5%",
                  width: "45%"
                }}
                value=""
            />
            <FormsyText
                floatingLabelText="Email"
                hintText="Email"
                name="email"
                required
                style={{
                  float: "right",
                  marginLeft: "5%",
                  width: "45%"
                }}
                validationError="Please enter a valid email address"
                validations="isEmail"
                value=""
            />
            <FormsySelect
                floatingLabelText="Candidate Position"
                name="position"
                required
                style={{
                  width: "100%"
                }}
            >
                <MenuItem
                    primaryText="PDR"
                    value={'HR'}
                />
                <MenuItem
                    primaryText="Frontend developer"
                    value={'FRONTEND_DEV'}
                />
                <MenuItem
                    primaryText="Backend developer"
                    value={'BACKEND_DEV'}
                />
                <MenuItem
                    primaryText="Tester"
                    value={'TESTER'}
                />
                <MenuItem
                    primaryText="Manager"
                    value={'MANAGER'}
                />
            </FormsySelect>
            <RaisedButton
                className="login-btn"
                disabled={!this.state.canSubmit}
                label="Add"
                required
                secondary
                style={{
                  float: "right",
                  marginTop: "2rem"
                }}
                type="submit"
            />
          </Formsy.Form>
        </Paper>
        <Snackbar
            autoHideDuration={4000}
            message="Candidate has been succesfully added in the system"
            onRequestClose={this.handleRequestClose}
            open={this.state.open}
        />
      </div>
    );
  }
});

export default Register;
