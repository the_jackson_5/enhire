import RegisterModel from './register-model';
import Baobab from 'baobab';

let registerOpts = {
  initialState: {},
  subCursors: {}
},
registerModel = RegisterModel({
  route: 'register',
  path: ['app', 'register'],
  options: registerOpts
});

export default registerModel;
