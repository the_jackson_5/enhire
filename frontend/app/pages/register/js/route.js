import Register from './register';
import constants from 'app/common/components/constants/js/constants';

export default {
  path: constants.registerUrl,
  getComponent(location, cb) {
    require.ensure([], () => {
      cb(null, Register.Class);
    });
  }
};
