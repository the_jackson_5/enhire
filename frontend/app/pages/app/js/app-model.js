import Stampit from 'stampit';
import BaseMemberModel from 'app/common/components/base-model/js/base-member';

let AppModel = Stampit
  .compose(BaseMemberModel);

export default AppModel;
