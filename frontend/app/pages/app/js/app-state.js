import AppModel from './app-model';

var appOpts = {
  initialState: {
    appName: 'ENhire'
  },
  subCursors: {
    appName: ['appName']
  }
};

const appContainerModel = AppModel({
  path: ['app'],
  options: appOpts
});

export default appContainerModel;
