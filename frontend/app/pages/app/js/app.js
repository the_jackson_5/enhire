import React from 'react';
import {Link} from 'react-router';
import {root, branch} from 'baobab-react/higher-order';

import browserHistory from 'app/common/components/history/js/history';
import StateTree from 'app/common/components/state-tree/js/state-tree';
import AppModel from './app-state';
import AuthModel from 'app/common/components/auth/js/auth';
import appStyles from '../sass/app.scss';
import Login from 'app/pages/login/js/login';
import Dashboard from 'app/pages/dashboard/js/dashboard';
import Menu from 'app/common/components/menu/js/menu';
import TopBar from 'app/common/components/top-bar/js/top-bar';

let AppContainer = {},
  AppCursors,
  tree;

AppContainer.Class = React.createClass({
  displayName: 'App',
  propTypes: {
    children: React.PropTypes.object,
    location: React.PropTypes.object,
    loggedIn: React.PropTypes.bool
  },
  componentWillMount(){
    let route = this.props.location;
    //secure application routes
    this.secured = true;

    if(route.pathname === 'feedback') {
      this.secured = false;
    }
  },
  componentWillUpdate(nextProps, nextState) {
    let route = this.props.location;
    //secure application routes
    this.secured = true;

    if(route.pathname === 'feedback') {
      this.secured = false;
    }
  },
  renderSecured() {
    return (this.props.loggedIn
        ? <section>
            <Menu.Class />
            <TopBar.Branch />
            {this.props.children || <Dashboard.Class />}
          </section>
        : <Login.Class />
      );
  },
  renderExternal() {
    return (this.props.children);
  },
  render() {
    return (
      <div className="app-container">
        {
          this.secured
            ? this.renderSecured()
            : this.renderExternal()
        }
      </div>
    );
  }
});

tree = StateTree.getTreeInstance();
AppCursors = Object.assign(AppModel.getSubCursors(), AuthModel.getSubCursors());

const branchedApp = branch(AppContainer.Class, {
  cursors: AppCursors
});

AppContainer.Branch = root(branchedApp, tree);

export default AppContainer;
