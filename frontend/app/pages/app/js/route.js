import App from './app';
import constants from 'app/common/components/constants/js/constants';
import RegisterRoute from 'app/pages/register/js/route';
import CandidatesRoute from 'app/pages/candidates/js/route';
import FeedbacksRoute from 'app/pages/feedbacks/js/route';
import CandidateFeedbackRoute from 'app/pages/candidate-feedback/js/route';

export default {
  component: App.Branch,
  path: constants.homeUrl,
  childRoutes: [
    RegisterRoute,
    CandidatesRoute,
    CandidateFeedbackRoute,
    FeedbacksRoute
  ]
};
