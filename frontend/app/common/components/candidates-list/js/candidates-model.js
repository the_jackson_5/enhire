import Stampit from 'stampit';
import BaseModel from 'app/common/components/base-model/js/base-collection';

let ComposedCandidatesList,
  CandidatesList;

CandidatesList = Stampit
.methods({
  saveDataToTree(data) {
    if (!data) {
      throw new TypeError('No candidates date received');
    }

    data.forEach((data, index) => {
      this.cursor.set(['list', index], data);
    });
    this.cursor.set('$loaded', true);
  }
});

ComposedCandidatesList = BaseModel.compose(CandidatesList);

export default ComposedCandidatesList;
