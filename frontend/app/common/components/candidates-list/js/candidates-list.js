import React from 'react';
import {Link} from 'react-router';
import {root, branch} from 'baobab-react/higher-order';

import browserHistory from 'app/common/components/history/js/history';
import Avatar from 'material-ui/lib/avatar';
import List from 'material-ui/lib/lists/list';
import ListItem from 'material-ui/lib/lists/list-item';
import Paper from 'material-ui/lib/paper';
import PersonIcon from 'material-ui/lib/svg-icons/social/person';
import DoneIcon from 'material-ui/lib/svg-icons/action/done';
import ClearIcon from 'material-ui/lib/svg-icons/content/clear';
import LinkIcon from 'material-ui/lib/svg-icons/content/link';
import CandidatesDef from './candidates-state';
import CandidatesModel from './candidates-model';
import StateTree from 'app/common/components/state-tree/js/state-tree';
import IconButton from 'material-ui/lib/icon-button';

let CandidatesList = {},
  CandidatesListModel;

CandidatesList.Class = React.createClass({
  displayName: 'CandidatesList',
  propTypes: {
    $loaded: React.PropTypes.bool,
    list: React.PropTypes.array,
    status: React.PropTypes.string
  },
  componentWillMount() {
    let tree = StateTree.getTreeInstance(),
      interviewerId = tree.get(['app', 'auth', 'interviewer_id']);

    this.interviewer_id = interviewerId;
  },
  hasGrade(arr) {
    let i = 0;

    for (i; i < arr.length; i++) {
      if (arr[i].employee_id === this.interviewer_id && arr[i].grade) {
        return true;
      }
    }

    return false;
  },
  getIcon(stagesArr) {
    let component;
    if(this.props.status === 'pending') {
      return null;
    }

    if(this.hasGrade(stagesArr)) {
      component = <DoneIcon style={{color: "green"}}/>;
    } else {
      component = <ClearIcon style={{color: "red"}}/>;
    }

    return component;
  },
  getIconButton(candidateData) {
    let button;

    if(this.props.status === 'pending') {
      return null;
    }

    button = (
      <IconButton
          onClick={this.goToFeedbackPage.bind(this, candidateData._id)}
          style={{
            marginRight: "5rem"
          }}
          tooltip="Go to feedback page"
      >
        <LinkIcon />
      </IconButton>);

    return button;
  },
  goToFeedbackPage(candidateId) {
    console.log(browserHistory);
    window.location.href = 'http://192.168.0.180/feedback?candidate_id=' + candidateId + '&interviewer_id=' + this.interviewer_id;
  },
  render() {
    return (
      this.props.$loaded
        ? <Paper
            className="register-form-wrapper"
            style={{
              float: "left",
              marginRight: "3%",
              overflow: "hidden",
              padding: "20px",
              width: "47%"
            }}
          >
            <List subheader={this.props.status.toUpperCase()}>
              {this.props.list.map((candidate, index) => {
                return (
                  <ListItem
                      key={index}
                      leftAvatar={<Avatar icon={<PersonIcon />} />}
                      primaryText={candidate.first_name + ' ' + candidate.last_name}
                      rightIcon={this.getIcon(candidate.current_application.stages)}
                      rightIconButton={this.getIconButton(candidate)}
                  />
                );
              })}

            </List>
          </Paper>
        : null
    );
  }
});

CandidatesList.Branch = branch(CandidatesList.Class, {
  cursors: (props, context) => {
    let queryParams = {};

    CandidatesDef.status = props.status;
    CandidatesDef.path.push('candidates_' + props.status);
    CandidatesListModel = CandidatesModel(CandidatesDef);

    queryParams.status = props.status;
    if(props.interviewer_id) {
      queryParams.interviewer_id = props.interviewer_id;
    }

    CandidatesListModel.getData(queryParams);


    return CandidatesListModel.getSubCursors();
  }
});

export default CandidatesList;
