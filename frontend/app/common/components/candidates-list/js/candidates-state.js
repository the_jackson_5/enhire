let CandidatesListDef = {
  status: '',
  route: 'candidates',
  path: ['app'],
  options: {
    initialState: {
      $loaded: false,
      list: []
    },
    subCursors: {
      list: ['list'],
      $loaded: ['$loaded']
    }
  }
};

export default CandidatesListDef;
