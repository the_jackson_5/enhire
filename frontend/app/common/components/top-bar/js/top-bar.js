import React from 'react';
import {Link} from 'react-router';
import {root, branch} from 'baobab-react/higher-order';
import FontIcon from 'material-ui/lib/font-icon';
import PersonIcon from 'material-ui/lib/svg-icons/social/person';
import Avatar from 'material-ui/lib/avatar';
import AppBar from 'material-ui/lib/app-bar';
import IconButton from 'material-ui/lib/icon-button';
import NavigationClose from 'material-ui/lib/svg-icons/navigation/close';
import IconMenu from 'material-ui/lib/menus/icon-menu';
import MoreVertIcon from 'material-ui/lib/svg-icons/navigation/more-vert';
import MenuItem from 'material-ui/lib/menus/menu-item';
import HomeIcon from 'material-ui/lib/svg-icons/action/home';
import AuthModel from 'app/common/components/auth/js/auth';

import InterviewerModel from 'app/common/components/interviewer-model/js/interviewer-model';
import InterviewerModelDef from 'app/common/components/interviewer-model/js/interviewer-model-def';

let TopBar = {},
  interviewerModelInstance;

TopBar.Class = React.createClass({
  displayName: 'TopBar',
  propTypes: {
    username: React.PropTypes.string
  },
  getInitialState() {
    return {
      interviewer:{}
    };
  },
  componentWillMount() {
    InterviewerModelDef.id = AuthModel.cursor.get('interviewer_id');
    interviewerModelInstance = InterviewerModel(InterviewerModelDef);
    interviewerModelInstance.getData(this.getInterviewerCallback);
  },
  getInterviewerCallback(data) {
    let fullName = 'Dashboard';

    if(data.first_name && data.last_name){
      fullName = data.first_name + " " + data.last_name;
    }

    AuthModel.cursor.set('username', fullName);
    this.setState({interviewer: data});
  },
  render() {
    return (
      <div className="top-bar">
        <AppBar
            iconElementLeft={<IconButton><HomeIcon /></IconButton>}
            iconElementRight={
              <IconMenu
                  anchorOrigin={{horizontal: 'right', vertical: 'top'}}
                  iconButtonElement={
                    <IconButton><MoreVertIcon /></IconButton>
                  }
                  targetOrigin={{horizontal: 'right', vertical: 'top'}}
              >
                <MenuItem
                    onClick={AuthModel.logout.bind(AuthModel)}
                    primaryText="Sign out"
                />
              </IconMenu>
            }
            title={this.props.username}
        />
      </div>
    );
  }
});

TopBar.Branch = branch(TopBar.Class, {
  cursors: AuthModel.getSubCursors()
});

export default TopBar;
