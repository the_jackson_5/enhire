import React from 'react';
import {Link} from 'react-router';
import {root, branch} from 'baobab-react/higher-order';
import LeftNav from 'material-ui/lib/left-nav';
import MenuItem from 'material-ui/lib/menus/menu-item';
import PersonAdd from 'material-ui/lib/svg-icons/social/person-add';
import PersonGroupIcon from 'material-ui/lib/svg-icons/social/group';
import CardTitle from 'material-ui/lib/card/card-title';
import Divider from 'material-ui/lib/divider';
import DashboarIcon from 'material-ui/lib/svg-icons/action/dashboard';
import FeedbackIcon from 'material-ui/lib/svg-icons/action/feedback';
import FontIcon from 'material-ui/lib/font-icon';
import AppBar from 'material-ui/lib/app-bar';
import browserHistory from 'app/common/components/history/js/history';
import constants from 'app/common/components/constants/js/constants';

let Menu = {};

Menu.Class = React.createClass({
  displayName: 'Menu',
  goToPage(page) {
    browserHistory.push(page);
  },
  render() {
    return (
      <nav className="main-menu">
        <LeftNav open>
          <CardTitle
              title="ENhire"
          />
          <MenuItem
              leftIcon={<DashboarIcon />}
              onClick={this.goToPage.bind(this, constants.homeUrl)}
              primaryText="Dashboard"
          />
          <Divider />
          <MenuItem
              leftIcon={<PersonAdd />}
              onClick={this.goToPage.bind(this, constants.registerUrl)}
              primaryText="Add Candidate"
          />
          <Divider />
          <MenuItem
              leftIcon={<PersonGroupIcon />}
              onClick={this.goToPage.bind(this, constants.candidatesUrl)}
              primaryText="Candidates"
          />
          <Divider />
          <MenuItem
              leftIcon={<FeedbackIcon />}
              onClick={this.goToPage.bind(this, constants.feedbacksUrl)}
              primaryText="Feedbacks"
          />
        </LeftNav>
      </nav>
    );
  }
});

export default Menu;
