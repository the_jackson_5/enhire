var CandidateModelDef = {
  route: 'candidates',
  path: ['candidate'],
  type: 'one',
  id: 0,
  options: {
    initialState: {},
    subCursors: {}
  }
};

export default CandidateModelDef;
