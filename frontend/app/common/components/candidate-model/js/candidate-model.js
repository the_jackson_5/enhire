import Stampit from 'stampit';
import BaseModel from 'app/common/components/base-model/js/base-member';

var CandidateModel = Stampit
  .init(function (){})
  .methods({
    saveDataToTree(data){
      if(!data) {
        return;
      }

      this.cursor.set(data);
    }
  });


export default BaseModel.compose(CandidateModel);
