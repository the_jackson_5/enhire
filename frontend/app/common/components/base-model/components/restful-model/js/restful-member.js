import Stampit from 'stampit';
import Restful, { fetchBackend } from 'restful.js';

let RestfulMemberModel = Stampit
  .refs({
    route: '',
    rootEndpoint: null,
    defaultEndpoint: Restful(CONF.API_URL, fetchBackend(fetch)),
    id: '',
    type: 'one' //'one' or 'custom'
  })
  .init(function() {
    this.endpoint = this._getEndpoint();
  })
  .methods({
    _getEndpoint() {
      let api = this.rootEndpoint || this.defaultEndpoint;

      if(this.type === 'one') {
        return api.one(this.route, this.id);
      }

      return api.custom(this.route);
    },
    get() {
      return this.endpoint.get.apply(null, arguments);
    },
    post() {
      return this.endpoint.post.apply(null, arguments);
    },
    put() {
      return this.endpoint.put.apply(null, arguments);
    },
    delete() {
      return this.endpoint.delete.apply(null, arguments);
    }
  });

export default RestfulMemberModel;
