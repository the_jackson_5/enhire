import Stampit from 'stampit';
import Restful, { fetchBackend } from 'restful.js';

let RestfulCollectionModel = Stampit
  .refs({
    route: '',
    rootEndpoint: null,
    defaultEndpoint: Restful(CONF.API_URL, fetchBackend(fetch))
  })

  .init(function() {
    this.endpoint = this._getEndpoint();
  })
  .methods({
    _getEndpoint() {
      if(!this.rootEndpoint) {
        return this.defaultEndpoint.all(this.route);
      }

      return this.rootEndpoint.all(this.route);
    },

    get() {
      return this.endpoint.getAll.apply(null, arguments);
    },

    getMember() {
      return this.endpoint.get.apply(null, arguments);
    },

    post() {
      return this.endpoint.post.apply(null, arguments);
    }
  });

export default RestfulCollectionModel;
