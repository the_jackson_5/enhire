import Stampit from 'stampit';
import StateTree from 'app/common/components/state-tree/js/state-tree';

 let StateModel = Stampit
  .refs(
    {
      path: [],
      options: {}
    }
  )
  .init(
    function() {
      this.initialize();
    }
  )
  .methods({
    initialize() {
      if (!(this.path instanceof Array)){
        throw new Error("initial path has to be specified");
      }

      this.key = this.path.join('.');
      this.initialState = this.options.initialState || {};

      this.parseNodes();
      if (!StateTree.checkPath(this.path)) {
        let localData = this.getLocalData();

        StateTree.createBranch(this.path, this.initialState, localData);
      }

      this.cursor = StateTree.getCursor(this.path);
    },
    parseNodes() {
      this.path.forEach((node, index) => {
        if (node.indexOf(':') !== -1) {
          this.createDynamicNode(index, node.slice(1));
        }
      });
    },
    createDynamicNode(index , name) {
      if (!this.options.props) {
        throw new Error ("props not given for path " + this.path.join("/"));
      }
      if (this.options.props.hasOwnProperty(name)) {
        this.path.splice(index, 1, this.options.props[name]);
      } else {
        throw new Error("prop not found for node " + name);
      }
    },
    saveToLocalData(options = {env: 'sessionStorage'}) {
      let environemnt = window[options.env];
      environemnt.setItem(this.key, JSON.stringify(this.cursor.serialize()));
    },
    getLocalData() {
      let localStorageData = localStorage.getItem(this.key),
        sessionStorageData = sessionStorage.getItem(this.key);

      if(sessionStorageData === 'undefined') {
        sessionStorageData = null;
      }

      return JSON.parse(sessionStorageData || localStorageData);
    },
    clearLocalStateData() {
      sessionStorage.removeItem(this.key);
      localStorage.removeItem(this.key);
    },
    clearState() {
      this.cursor.set(this.initialState);
    },
    getSubCursors() {
      let subCursors = {},
          keys;

      if (!this.options.subCursors) {
        return false;
      }

      keys = Object.keys(this.options.subCursors);

      keys.forEach((value) => {
        subCursors[value] = this.path.concat(this.options.subCursors[value]);
      });

      return subCursors;
    }
  });

export default StateModel;
