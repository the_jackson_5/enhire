import Stampit from 'stampit';
import StateModel from '../components/state-model/js/state-model';

let BaseModel = Stampit
  .methods({
    _requestData(methodName, ...options) {
      let promise = this[methodName].apply(this, options),
        lastParam = options[options.length - 1],
        reqCallback = (typeof lastParam === 'function') ? lastParam : null;

      promise.then(
        this.successResponse.bind(this, reqCallback),
        this.errorResponse.bind(this, reqCallback)
      );

      return promise;
    },

    getData() {
      return this._requestData.apply(this, ['get'].concat(Array.from(arguments)));
    },

    postData() {
      return this._requestData.apply(this, ['post'].concat(Array.from(arguments)));
    },

    putData() {
      return this._requestData.apply(this, ['put'].concat(Array.from(arguments)));
    },

    successResponse(cb, rsp) {
      let rspBody = rsp.body(false),
        rspHeaders = rsp.headers();

      this.saveDataToTree(rspBody, rspHeaders);

      if(cb && typeof cb === 'function') {
        cb(rspBody, rspHeaders);
      }

      if(rspBody.error) {
        throw new TypeError(rspBody.error.code + ": " + rspBody.error.message);
      }
    },
    errorResponse(cb, err) {
      if(cb && typeof cb === 'function') {
        cb({
          error: {
            message: err
          }
        });
      }
    },

    saveDataToTree(data) {
      return data;
    }
  }).compose(StateModel);

export default BaseModel;
