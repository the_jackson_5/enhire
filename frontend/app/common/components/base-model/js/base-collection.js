import Stampit from 'stampit';
import RestfulCollection from '../components/restful-model/js/restful-collection';
import BaseModel from './base-model';

let BaseCollectionModel = Stampit
  .compose(BaseModel, RestfulCollection);

export default BaseCollectionModel;
