import Stampit from 'stampit';
import RestfulMember from '../components/restful-model/js/restful-member';
import BaseModel from './base-model';

let BaseMemberModel = Stampit
  .compose(BaseModel, RestfulMember);

export default BaseMemberModel;
