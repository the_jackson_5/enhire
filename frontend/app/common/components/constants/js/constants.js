const globals = {
  //URLs
  homeUrl: '/',
  registerUrl: '/register',
  candidatesUrl: '/candidates',
  feedbacksUrl: '/feedbacks',
  pageNotFoundUrl: '*',
  candidateFeedbackURL: '/feedback'
};

export default globals;
