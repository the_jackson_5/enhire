import Baobab from 'baobab';
import Stampit from 'stampit';

let StateTree = Stampit
  .refs({
    treeDef: {}
  })
  .init(function() {
    this.initialize();
  })
  .methods({
    initialize() {
      this.tree = new Baobab(this.treeDef);
      this.bkpTree = new Baobab(this.treeDef);
    },
    getTreeInstance() {
      return this.tree;
    },
    checkPath(path) {
      return this.tree.exists(path);
    },
    getCursor(path) {
      return this.tree.select(path);
    },
    createBranch(path, initialState, localData) {
      this.tree.set(path, localData || initialState);
      this.bkpTree.set(path, initialState);

      this.tree.commit();
      this.bkpTree.commit();
    },
    clearTree() {
      this.tree.set(this.bkpTree.serialize());
    }
  });

export default StateTree();
