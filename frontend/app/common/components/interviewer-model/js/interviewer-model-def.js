var InterviewerModelDef = {
  route: 'interviewers',
  path: ['interviewer'],
  type: 'one',
  id: 0,
  options: {
    initialState: {},
    subCursors: {}
  }
};

export default InterviewerModelDef;
