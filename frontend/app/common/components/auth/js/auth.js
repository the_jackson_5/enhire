import Stampit from 'stampit';
import StateTree from 'app/common/components/state-tree/js/state-tree';
import BaseModel from 'app/common/components/base-model/js/base-member';
import {browserHistory} from 'app/common/components/history/js/history';
import constants from 'app/common/components/constants/js/constants';

let AuthModel = Stampit
.methods({
  _clear() {
    this.clearLocalStateData();
    StateTree.clearTree();
  },
  logout() {
    this._clear();
    browserHistory.push(constants.homeUrl);
  },
  isLoggedIn() {
    return this.cursor.get('loggedIn');
  },
  saveDataToTree(data) {
    if (!data) {
      return;
    }

    this.cursor.set('interviewer_id', data.id || 1);
    this.cursor.set('loggedIn', true);
    this.saveToLocalData();
  }
});

let ComposedAuthModel = BaseModel.compose(AuthModel);

const auth = ComposedAuthModel ({
  route: 'login',
  type: 'custom',
  path: ['app', 'auth'],
  doNotInterceptRequests: true,
  doNotInterceptErrors: true,
  options: {
    initialState: {
      username: '',
      loggedIn: false,
      interviewer_id: null
    },
    subCursors: {
      loggedIn: ['loggedIn'],
      username: ['username'],
      interviewer_id: ['interviewer_id']
    }
  }
});

export default auth;
