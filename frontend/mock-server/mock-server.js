require('../files.js');

var jsonExpressServer = require('json-server'),
  serverInstance = jsonExpressServer.create(),
  router;

// Set default middlewares (logger, static, cors and no-cache)
serverInstance.use(jsonExpressServer.defaults());
routerInstance = jsonExpressServer.router(global.files.mockServer.database);
serverInstance.use(routerInstance);

serverInstance.listen(4444);
console.log('Server listening at 4444');
