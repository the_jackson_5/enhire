require('./files');

var gulp = require('gulp'),
  utils = require('./gulp/utils.js');

gulp.task('webpack', utils.getTask({taskName: 'webpack'}));
gulp.task('webpack-server', utils.getTask({taskName: 'webpack-server'}));

gulp.task('generate-db', utils.getTask({taskName: 'generate-db'}));
gulp.task('start-mock-server', ['generate-db'], utils.getTask({taskName: 'mock-server'}));
