package tests;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class LoginPage {

	private WebDriver driver;

	public LoginPage(WebDriver driver) {
		this.driver = driver;
	}

	public WebElement getUserName() {
		return driver.findElement(By.xpath("//input[@name='email']"));
	}

	public WebElement getPassword() {
		return driver.findElement(By.xpath("//input[@name='password']"));
	}

	public WebElement getLogin() {
		return driver.findElement(By.xpath("//*[text()='Login']"));
	}

	public void waitToLoad() {
		new WebDriverWait(driver, 3)
				.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@name='email']")));

	}

	public DashboardPage login(String existingemail, String password) {
		LoginPage loginPage = new LoginPage(driver);
		loginPage.waitToLoad();
		loginPage.getUserName().sendKeys(existingemail);
		loginPage.getPassword().sendKeys(password);
		loginPage.getLogin().click();
		DashboardPage dashBoard = new DashboardPage(driver);
		dashBoard.waitToLoad();
		Assert.assertTrue("DashBoard ", dashBoard.title().isDisplayed());
		return dashBoard;

	}
}
