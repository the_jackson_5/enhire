package tests;

import org.bson.Document;
import org.json.JSONObject;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;

public class DBConnection {

	public static String deleteCandidateFromDB(String candidate) {
		MongoClient mongoClient = new MongoClient("192.168.0.180");
		MongoDatabase database = mongoClient.getDatabase("enhire");
		MongoCollection<Document> collection = database.getCollection("candidates");
		MongoCursor<Document> cursor = collection.find().iterator();
		try {
			while (cursor.hasNext()) {
				Document item = cursor.next();
				if (new JSONObject(item.toJson()).getString("email").equals(candidate)) {
					System.out.println(new JSONObject(item.toJson()).getString("email"));
					collection.deleteOne(item);
				}

			}
		} finally {
			cursor.close();
		}
		System.out.println(collection.count());
		mongoClient.close();
		return null;
	}

	public static long getCandidates() {
		MongoClient mongoClient = new MongoClient("192.168.0.180");
		MongoDatabase database = mongoClient.getDatabase("enhire");
		MongoCollection<Document> collection = database.getCollection("candidates");
		long result = collection.count();
		mongoClient.close();
		return result;
	}

	public static String getInterviewer(String id) {
		MongoClient mongoClient = new MongoClient("192.168.0.180");
		MongoDatabase database = mongoClient.getDatabase("enhire");
		MongoCollection<Document> collection = database.getCollection("interviewers");
		MongoCursor<Document> cursor = collection.find().iterator();
		String result = null;
		try {
			while (cursor.hasNext()) {
				Document item = cursor.next();
				if (new JSONObject(item.toJson()).toString().contains(id)) {
					System.out.println(new JSONObject(item.toJson()));
					result = item.toJson();
				}

			}
		} finally {
			cursor.close();
		}
		mongoClient.close();
		return result;
	}

	public static String getCandidate(String email) {
		MongoClient mongoClient = new MongoClient("192.168.0.180");
		MongoDatabase database = mongoClient.getDatabase("enhire");
		MongoCollection<Document> collection = database.getCollection("candidates");
		MongoCursor<Document> cursor = collection.find().iterator();
		String result = "";
		try {
			while (cursor.hasNext()) {
				Document item = cursor.next();
				if (new JSONObject(item.toJson()).toString().contains(email)) {
					System.out.println(new JSONObject(item.toJson()));
					result = item.toJson();
				}

			}
		} finally {
			cursor.close();
		}
		mongoClient.close();
		return result;
	}
}
