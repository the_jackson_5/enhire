package tests;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;

import org.json.*;

import junit.framework.Assert;

public class RequestHandler {

	private URL requestUrl;
	private HttpURLConnection urlc;

	public RequestHandler(String method, String url, String parameters) throws IOException {
		requestUrl = new URL(url);
		urlc = (HttpURLConnection) requestUrl.openConnection();
		urlc.setDoOutput(true);
		urlc.setAllowUserInteraction(false);
		urlc.setRequestMethod(method);
		urlc.setRequestProperty("Content-Type", "application/json");
		if (parameters != null) {
			PrintStream ps = new PrintStream(urlc.getOutputStream());
			ps.print(parameters);
			ps.close();
		}
	}

	public int getResponseCode() throws IOException {
		return urlc.getResponseCode();
	}

	public JSONObject getResponse() throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(urlc.getInputStream()));
		String l = null;
		String result = "";
		while ((l = br.readLine()) != null) {
			result += l;
		}
		br.close();
		System.out.println(result);
		return new JSONObject(result);
	}

	public static JSONObject getRequest(String url, String parameters) throws IOException {
		URL requestUrl = new URL(url);
		URLConnection urlc = requestUrl.openConnection();
		urlc.setDoOutput(true);
		urlc.setAllowUserInteraction(false);
		BufferedReader br = new BufferedReader(new InputStreamReader(urlc.getInputStream()));
		String l = null;
		String result = "";
		while ((l = br.readLine()) != null) {
			result += l;
		}
		br.close();
		return new JSONObject(result);
	}

	public static int postRequest(String url, String parameters) throws IOException {
		URL requestUrl = new URL(url);

		HttpURLConnection urlc = (HttpURLConnection) requestUrl.openConnection();
		urlc.setRequestMethod("POST");
		urlc.setDoOutput(true);
		urlc.setDoInput(true);
		urlc.setRequestProperty("Content-Type", "application/json");
		urlc.setAllowUserInteraction(false);
		PrintStream ps = new PrintStream(urlc.getOutputStream());
		ps.print(parameters);
		ps.close();
		return urlc.getResponseCode();

	}

	public static JSONObject getPostResponseRequest(String url, String parameters) throws IOException {
		URL requestUrl = new URL(url);

		HttpURLConnection urlc = (HttpURLConnection) requestUrl.openConnection();
		urlc.setRequestMethod("POST");
		urlc.setDoOutput(true);
		urlc.setDoInput(true);
		urlc.setRequestProperty("Content-Type", "application/json");
		urlc.setAllowUserInteraction(false);
		PrintStream ps = new PrintStream(urlc.getOutputStream());
		ps.print(parameters);
		ps.close();
		int response = urlc.getResponseCode();
		Assert.assertEquals("Response code is 201", 201, response);
		BufferedReader br = new BufferedReader(new InputStreamReader(urlc.getInputStream()));
		String l = null;
		String result = "";
		while ((l = br.readLine()) != null) {
			result += l;
		}
		br.close();
		System.out.println(result);
		return new JSONObject(result);
	}

	public static String createJson(HashMap<String, String> map) {
		String result = "{";
		for (String item : map.keySet()) {
			if (!result.equals("{")) {
				result += ",";
			}
			result += "\"" + item + "\":\"" + map.get(item) + "\"";
		}
		result += "}";
		System.out.println(result);
		return result;

	}
}
