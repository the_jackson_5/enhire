package tests;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class DashboardPage {
	private WebDriver driver;

	public DashboardPage(WebDriver driver) {
		this.driver = driver;
	}

	public void waitToLoad() {
		try {
			new WebDriverWait(driver, 5).pollingEvery(300, TimeUnit.MILLISECONDS)
					.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h1")));
		} catch (Exception e) {
		}
	}

	public WebElement title() {
		return driver.findElement(By.xpath("//h1"));

	}

	public AddCandidatePage goToAddCandidate() {
		getAddCandidate().click();
		AddCandidatePage page = new AddCandidatePage(driver);
		page.waitToLoad();
		return page;
	}

	private WebElement getAddCandidate() {
		return driver.findElement(By.xpath("//div[text()='Add Candidate']"));
	}
}
