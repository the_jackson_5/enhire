package tests;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class AddCandidatePage {
	private WebDriver driver;

	public AddCandidatePage(WebDriver driver) {
		this.driver = driver;
	}

	public void waitToLoad() {
		try {
			new WebDriverWait(driver, 3).until(ExpectedConditions.visibilityOfElementLocated(
					By.xpath("//div[@title='Add candidate']/span[text()='Add candidate']")));
		} catch (Exception e) {
		}
	}

	public WebElement getFirstName() {
		return driver.findElement(By.xpath("//input[@name='first_name']"));
	}

	public WebElement getLastName() {
		return driver.findElement(By.xpath("//input[@name='last_name']"));
	}

	public WebElement getPhone() {
		return driver.findElement(By.xpath("//input[@name='phone']"));
	}

	public WebElement getEmail() {
		return driver.findElement(By.xpath("//input[@name='email']"));
	}

	public WebElement getPosition() {
		return driver.findElement(By.xpath("//div[@name='position']/div[1]"));
	}

	public void selectPosition(String position) throws InterruptedException {
		getPosition().click();
		Thread.sleep(1000);
		driver.findElement(By.xpath("//div[contains(@style,'z-index: 1000')]//*[text()='" + position + "']")).click();
		Thread.sleep(1000);
	}

	public WebElement getAdd() {
		return driver.findElement(By.xpath("//span[text()='Add']"));

	}

}
