package com.enhire;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import com.enhire.login.LoginIntegration;
import com.enhire.registration.RegisterIntegration;

@RunWith(Suite.class)
@SuiteClasses({ LoginIntegration.class,RegisterIntegration.class})
public class IntegrationTestCase {

}
