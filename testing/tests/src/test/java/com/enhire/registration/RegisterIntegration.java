package com.enhire.registration;

import org.json.JSONObject;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.chrome.ChromeDriver;

import com.tngtech.java.junit.dataprovider.DataProvider;
import com.tngtech.java.junit.dataprovider.DataProviderRunner;
import com.tngtech.java.junit.dataprovider.UseDataProvider;

import tests.AddCandidatePage;
import tests.DBConnection;
import tests.DashboardPage;
import tests.LoginPage;

@RunWith(DataProviderRunner.class)
public class RegisterIntegration {
	ChromeDriver driver;
	private static final String EXISTINGEMAIL = "sergiu.lucutar@endava.com";
	private static final String GOOD_PASSWORD = "very_stronk";
	public static String NEWEMAIL = "gica88@gmail.com";

	@Before
	public void initialize() {
		driver = new ChromeDriver();
		driver.get("http://192.168.0.180/");
	}

	@After
	public void finalize() throws InterruptedException {
		driver.quit();
		driver = null;
		Thread.sleep(5000);
	}

	@DataProvider
	public static Object[][] position() {
		return new Object[][] { { "PDR", "HR" }, { "Frontend developer", "FRONTEND_DEV" },
				{ "Backend developer", "BACKEND_DEV" }, { "Tester", "TESTER" }, { "Manager", "MANAGER" } };
	}

	@Test
	@UseDataProvider("position")
	public void testNewCandidate(String setPositin, String dbPosition) throws InterruptedException {
		DBConnection.deleteCandidateFromDB(NEWEMAIL);
		LoginPage loginPage = new LoginPage(driver);
		DashboardPage dashBoard = loginPage.login(EXISTINGEMAIL, GOOD_PASSWORD);
		AddCandidatePage candidate = dashBoard.goToAddCandidate();
		candidate.getFirstName().sendKeys("Gica");
		candidate.getLastName().sendKeys("88");
		candidate.getPhone().sendKeys("0738989898");
		candidate.getEmail().sendKeys(NEWEMAIL);
		candidate.selectPosition(setPositin);
		candidate.getAdd().click();
		Thread.sleep(5000);
		JSONObject dbObject = new JSONObject(DBConnection.getCandidate(NEWEMAIL));
		Assert.assertNotNull("Candidate added to DB ", dbObject);
		Assert.assertEquals("first_name is corect ", "Gica", dbObject.getString("first_name"));
		Assert.assertEquals("last_name is corect ", "88", dbObject.getString("last_name"));
		Assert.assertEquals("phone is corect ", "0738989898", dbObject.getString("phone"));
		Assert.assertEquals("email is corect ", NEWEMAIL, dbObject.getString("email"));
		Assert.assertEquals("position is corect ", dbPosition,
				dbObject.getJSONObject("current_application").getString("function"));
	}

	@DataProvider
	public static Object[][] wrongfields() {
		return new Object[][] { { EXISTINGEMAIL, "" }, { "", GOOD_PASSWORD }, { "BadEmail", GOOD_PASSWORD },
				{ EXISTINGEMAIL, "badPass" } };
	}

	@Test
	@UseDataProvider("wrongfields")
	public void testUnSuccesfullyLogin(String user, String password) {
		LoginPage loginPage = new LoginPage(driver);
		loginPage.waitToLoad();
		loginPage.getUserName().sendKeys(user);
		loginPage.getPassword().sendKeys(password);
		loginPage.getLogin().click();
		DashboardPage dashBoard = new DashboardPage(driver);
		dashBoard.waitToLoad();
		Assert.assertTrue("DashBoard ", loginPage.getUserName().isDisplayed());
	}

}
