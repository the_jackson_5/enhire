package com.enhire.registration;

import java.io.IOException;
import java.util.LinkedHashMap;

import org.json.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.tngtech.java.junit.dataprovider.DataProvider;
import com.tngtech.java.junit.dataprovider.DataProviderRunner;
import com.tngtech.java.junit.dataprovider.UseDataProvider;

import junit.framework.Assert;
import tests.DBConnection;
import tests.RequestHandler;

@RunWith(DataProviderRunner.class)
public class RegisterEndpoint {
	public static String LINK = "http://192.168.0.180:4444/register";
	public static String EXISTINGEMAIL = "test10@email.com";
	public static String NEWEMAIL = "test9@email.com";

	@DataProvider
	public static Object[][] position() {
		return new Object[][] { { "HR" }, { "FRONTEND_DEV" }, { "BACKEND_DEV" }, { "TESTER" }, { "MANAGER" } };
	}

	@Test
	@UseDataProvider("position")
	public void testNewCandidate(String position) throws IOException {
		DBConnection.deleteCandidateFromDB(NEWEMAIL);
		LinkedHashMap<String, String> map = new LinkedHashMap<String, String>();
		map.put("email", NEWEMAIL);
		map.put("first_name", "Nume");
		map.put("last_name", "Prenume");
		map.put("phone", "0737304555");
		map.put("position", position);
		String parameters = RequestHandler.createJson(map);
		JSONObject response = RequestHandler.getPostResponseRequest(LINK, parameters);
		Assert.assertEquals("Service response is correct", response.toString(),
				"{\"code\":200,\"message\":\"Candidate successfully registered\"}");
		JSONObject dbObject = new JSONObject(DBConnection.getCandidate(NEWEMAIL));
		Assert.assertEquals("position is corect ", position,
				dbObject.getJSONObject("current_application").getString("function"));


	}

	@Test
	public void testExistingCandidate() throws IOException {
		LinkedHashMap<String, String> map = new LinkedHashMap<String, String>();
		map.put("email", EXISTINGEMAIL);
		map.put("first_name", "Nume");
		map.put("last_name", "Prenume");
		map.put("phone", "02345");
		map.put("position", "HR");
		String parameters = RequestHandler.createJson(map);
		int response = RequestHandler.postRequest(LINK, parameters);
		Assert.assertEquals("Service response is correct", 409, response);

	}

	@Test
	public void testCandidateNoField() throws IOException {
		long initial = DBConnection.getCandidates();
		LinkedHashMap<String, String> map = new LinkedHashMap<String, String>();
		String parameters = RequestHandler.createJson(map);
		int response = RequestHandler.postRequest(LINK, parameters);

		Assert.assertEquals("Service response is correct", 400, response);
		Assert.assertEquals("No record added in DB", initial, DBConnection.getCandidates());

	}

	@Test
	public void testCandidateOnlyEmail() throws IOException {
		DBConnection.deleteCandidateFromDB(NEWEMAIL);
		long initial = DBConnection.getCandidates();
		LinkedHashMap<String, String> map = new LinkedHashMap<String, String>();
		map.put("email", NEWEMAIL);
		String parameters = RequestHandler.createJson(map);
		int response = RequestHandler.postRequest(LINK, parameters);

		Assert.assertEquals("Service response is correct", 400, response);
		Assert.assertEquals("No record added in DB", initial, DBConnection.getCandidates());

	}

	@Test
	public void testCandidateOnlyEmailAndName() throws IOException {
		DBConnection.deleteCandidateFromDB(NEWEMAIL);
		long initial = DBConnection.getCandidates();
		LinkedHashMap<String, String> map = new LinkedHashMap<String, String>();
		map.put("email", NEWEMAIL);
		map.put("first_name", "Nume");
		String parameters = RequestHandler.createJson(map);
		int response = RequestHandler.postRequest(LINK, parameters);

		Assert.assertEquals("Service response is correct", 400, response);
		Assert.assertEquals("No record added in DB", initial, DBConnection.getCandidates());

	}

	@Test
	public void testCandidateNoPhone() throws IOException {
		DBConnection.deleteCandidateFromDB(NEWEMAIL);
		long initial = DBConnection.getCandidates();
		LinkedHashMap<String, String> map = new LinkedHashMap<String, String>();
		map.put("email", NEWEMAIL);
		map.put("first_name", "Nume");
		map.put("last_name", "Prenume");
		String parameters = RequestHandler.createJson(map);
		int response = RequestHandler.postRequest(LINK, parameters);
		Assert.assertEquals("Service response is correct", 400, response);
		Assert.assertEquals("No record added in DB", initial, DBConnection.getCandidates());

	}

}
