package com.enhire.interviewers;

import java.io.IOException;

import org.apache.http.HttpStatus;
import org.json.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.tngtech.java.junit.dataprovider.DataProvider;
import com.tngtech.java.junit.dataprovider.DataProviderRunner;
import com.tngtech.java.junit.dataprovider.UseDataProvider;

import junit.framework.Assert;
import tests.DBConnection;
import tests.RequestHandler;

@RunWith(DataProviderRunner.class)
public class InterviewersEndpint {
	public static String ENDPOINT = "http://192.168.0.180:4444/interviewers";

	@DataProvider
	public static Object[][] ids() {
		return new Object[][] { { "5712a2fd9249f8e92843464c", HttpStatus.SC_OK },
				{ "5712a2fd9249f8e92843464d", HttpStatus.SC_OK }, { "abcdef", HttpStatus.SC_NOT_FOUND } };
	}

	@Test
	@UseDataProvider("ids")
	public void testInterviewersResult(String interviewerID, int statusode) throws IOException {
		RequestHandler request = new RequestHandler("GET", ENDPOINT + "/" + interviewerID, null);
		Assert.assertEquals("ResponseCode is correct", statusode, request.getResponseCode());

	}

	@Test
	public void testData() throws IOException {
		String fromDB = DBConnection.getInterviewer("5712a2fd9249f8e92843464c");
		RequestHandler request = new RequestHandler("GET", ENDPOINT + "/" + "5712a2fd9249f8e92843464c", null);
		JSONObject expected = new JSONObject(fromDB);
		JSONObject actual = request.getResponse();
		Assert.assertEquals("first_name is correct", expected.getString("first_name"), actual.getString("first_name"));
		Assert.assertEquals("last_name is correct", expected.getString("last_name"), actual.getString("last_name"));
		Assert.assertEquals("email is correct", expected.getString("email"), actual.getString("email"));
	}
}
