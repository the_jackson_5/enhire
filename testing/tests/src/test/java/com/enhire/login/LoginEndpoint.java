package com.enhire.login;

import java.io.IOException;
import java.util.LinkedHashMap;

import org.apache.http.HttpStatus;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.tngtech.java.junit.dataprovider.DataProvider;
import com.tngtech.java.junit.dataprovider.DataProviderRunner;
import com.tngtech.java.junit.dataprovider.UseDataProvider;

import junit.framework.Assert;
import tests.RequestHandler;

@RunWith(DataProviderRunner.class)
public class LoginEndpoint {
	private static final String EXISTINGEMAIL = "sergiu.lucutar@endava.com";
	private static final String GOOD_PASSWORD = "very_stronk";

	public static String ENDPOINT = "http://192.168.0.180:4444/login";

	@Test
	public void testSuccesfullLogin() throws IOException {
		LinkedHashMap<String, String> map = new LinkedHashMap<String, String>();
		map.put("email", EXISTINGEMAIL);
		map.put("password", GOOD_PASSWORD);
		String parameters = RequestHandler.createJson(map);
		RequestHandler request = new RequestHandler("POST", ENDPOINT, parameters);
		Assert.assertEquals("ResponseCode is correct", HttpStatus.SC_OK, request.getResponseCode());
		Assert.assertEquals("Service is up", "5712a2fd9249f8e92843464d", request.getResponse().getString("id"));
	}

	@DataProvider
	public static Object[][] wrongfields() {
		return new Object[][] { { EXISTINGEMAIL, "", HttpStatus.SC_BAD_REQUEST },
				{ "", GOOD_PASSWORD, HttpStatus.SC_BAD_REQUEST },
		{ "BadEmail", GOOD_PASSWORD, HttpStatus.SC_UNAUTHORIZED },
		{ EXISTINGEMAIL, "badPass", HttpStatus.SC_UNAUTHORIZED }};
	}

	@Test
	@UseDataProvider("wrongfields")
	public void testWrongPassword(String user, String password, int staus) throws IOException {
		LinkedHashMap<String, String> map = new LinkedHashMap<String, String>();
		map.put("email", user);
		map.put("password", password);
		String parameters = RequestHandler.createJson(map);
		RequestHandler request = new RequestHandler("POST", ENDPOINT, parameters);
		Assert.assertEquals("ResponseCode is correct", staus, request.getResponseCode());
	}

}
