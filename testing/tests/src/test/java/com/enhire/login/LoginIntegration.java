package com.enhire.login;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.chrome.ChromeDriver;

import com.tngtech.java.junit.dataprovider.DataProvider;
import com.tngtech.java.junit.dataprovider.DataProviderRunner;
import com.tngtech.java.junit.dataprovider.UseDataProvider;

import tests.DashboardPage;
import tests.LoginPage;

@RunWith(DataProviderRunner.class)
public class LoginIntegration {
	ChromeDriver driver;
	private static final String EXISTINGEMAIL = "sergiu.lucutar@endava.com";
	private static final String GOOD_PASSWORD = "very_stronk";

	@Before
	public void initialize() throws InterruptedException {
		Thread.sleep(3000);
		driver = new ChromeDriver();
		driver.get("http://192.168.0.180/");
	}

	@After
	public void finalize() {
		driver.quit();
		driver = null;
	}

	@Test
	public void testSuccesfullyLogin() {
		LoginPage loginPage = new LoginPage(driver);
		loginPage.waitToLoad();
		loginPage.getUserName().sendKeys(EXISTINGEMAIL);
		loginPage.getPassword().sendKeys(GOOD_PASSWORD);
		loginPage.getLogin().click();
		DashboardPage dashBoard = new DashboardPage(driver);
		dashBoard.waitToLoad();
		Assert.assertTrue("DashBoard ", dashBoard.title().isDisplayed());
	}

	@DataProvider
	public static Object[][] wrongfields() {
		return new Object[][] { { EXISTINGEMAIL, "" }, { "", GOOD_PASSWORD }, { "BadEmail", GOOD_PASSWORD },
				{ EXISTINGEMAIL, "badPass" } };
	}

	@Test
	@UseDataProvider("wrongfields")
	public void testUnSuccesfullyLogin(String user, String password) {
		LoginPage loginPage = new LoginPage(driver);
		loginPage.waitToLoad();
		loginPage.getUserName().sendKeys(user);
		loginPage.getPassword().sendKeys(password);
		loginPage.getLogin().click();
		DashboardPage dashBoard = new DashboardPage(driver);
		dashBoard.waitToLoad();
		Assert.assertTrue("DashBoard is displayed", loginPage.getUserName().isDisplayed());
	}

}
