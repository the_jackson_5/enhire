package com.enhire;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import com.enhire.interviewers.InterviewersEndpint;
import com.enhire.login.LoginEndpoint;
import com.enhire.registration.RegisterEndpoint;

@RunWith(Suite.class)
@SuiteClasses({ LoginEndpoint.class, RegisterEndpoint.class, InterviewersEndpint.class })
public class BackEndTestCase {

}
